# Linux Router with SystemD NetworkD

## Disable Debian's server network management

Modify /etc/network/interfaces to only have the following. This is so that SystenD NetworkD can take control of all the physical interfaces.

	# This file describes the network interfaces available on your system
	# and how to activate them. For more information, see interfaces(5).

	source /etc/network/interfaces.d/*

	# The loopback network interface
	auto lo
	iface lo inet loopback	

## Setup firewall rules.

Create the file '/usr/local/sbin/ipreset-configs' and add the following contents:

	#!/bin/bash

	# Enable filtering module to filter bridge traffic.
	# Without you can end up in a situation where traffic between interfaces on the same bridge are allowed through despite there being explicit rules to block it.

		modprobe br_netfilter

	# Flush all IPv4 and IPv6 rules.

		# v4
			iptables -F
			iptables -X
			iptables -t nat -F
			iptables -t nat -X
			iptables -t mangle -F
			iptables -t mangle -X
			iptables -P INPUT ACCEPT
			iptables -P FORWARD ACCEPT
			iptables -P OUTPUT ACCEPT

		# v6
			ip6tables -F

	# NAT any traffic destined to leave the 'wan' interface.

		iptables --table nat --append POSTROUTING --out-interface wan -j MASQUERADE

	# Allow traffic that's passing through the firewall from one interface to another if it is part of an established connection.

	# If traffic is part of an existing session, let it through.
	# By default IPTables will look at each packet individually and not take sessions in to account.
	# If a device on the LAN wanted to download a web page from a device on the WAN, then it would need to send a request to the WAN (allowed) and the device on the WAN would need to respond (disallowed).
	# To allow the device on the LAN to download a web page without allowing all traffic on the WAN, you can tell IPTables to allow traffic from the WAN only if it is part of an existing session.
	# That's what these settings do.

			# v4

				iptables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT
			# v6
				ip6tables -A FORWARD -m state --state ESTABLISHED,RELATED -j ACCEPT

	# Allow established incoming connections to this machine.
	# This is similar to the rule above but applies to traffic destined to the router itself rather than traffic that is passing through the router to get to another device.

		# v4
			iptables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

		# v6
			ip6tables -A INPUT -m conntrack --ctstate RELATED,ESTABLISHED -j ACCEPT

	# Deny traffic to private addresses on Internet for all interfaces.

		iptables -A FORWARD -o wan -d 10.0.0.0/8 -j REJECT
		iptables -A FORWARD -o wan -d 192.168.0.0/16 -j REJECT
		iptables -A FORWARD -o wan -d 172.16.0.0/12 -j REJECT

	# Deny from traffic from LAN to Management.

		iptables -A FORWARD -i lan -o mgt -j REJECT --reject-with icmp-admin-prohibited
		ip6tables -A FORWARD -i lan -o mgt -j REJECT

	# Deny all traffic from the WAN.

		iptables -A FORWARD -i wan -j REJECT --reject-with icmp-admin-prohibited
		ip6tables -A FORWARD -i wan -j REJECT

	# Allow all communication to/from the loopback interface.

		# v4
			iptables -A INPUT -i lo -j ACCEPT
			iptables -A OUTPUT -o lo -j ACCEPT

		# v6
			ip6tables -A INPUT -i lo -j ACCEPT
			ip6tables -A OUTPUT -o lo -j ACCEPT


	# Allow ICMP pinging for everything.

		# v4
			iptables -A INPUT -p icmp -j ACCEPT
			iptables -A OUTPUT -p icmp -j ACCEPT
			iptables -A FORWARD -p icmp -j ACCEPT

		# v6.
			ip6tables -A INPUT -p ipv6-icmp -j ACCEPT
			ip6tables -A OUTPUT -p ipv6-icmp -j ACCEPT
			ip6tables -A FORWARD -p ipv6-icmp -j ACCEPT

	# Allow access to the D.N.S. server on this machine.

		# v4
			iptables -A INPUT -p tcp -m tcp --dport 53 -j ACCEPT
			iptables -A INPUT -p udp -m udp --dport 53 -j ACCEPT
			iptables -A INPUT -p udp -m udp --dport 67 -j ACCEPT

		# v6
			ip6tables -A INPUT -p tcp -m tcp --dport 53 -j ACCEPT
			ip6tables -A INPUT -p udp -m udp --dport 53 -j ACCEPT
			ip6tables -A INPUT -p udp -m udp --dport 547 -j ACCEPT

	# Allow access to the S.S.H. server on this machine.

		# v4
			iptables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT

		# v6
			ip6tables -A INPUT -p tcp -m tcp --dport 22 -j ACCEPT


	# Block all traffic to this machine (which does not match one of the rules above).
	# This only applies to traffic where this machine is the intended destination and not traffic forwarded by this machine.

		# v4
			iptables -P INPUT DROP

		# v6

			ip6tables -P INPUT DROP

Run the following command to make the file executable:

	chmod +x /usr/local/sbin/ipreset-configs

Create the following file: '/etc/systemd/system/ipreset.service' and add the following contents:

	[Unit]
	Description=IPreset - Configure Firewall
	Before=network-online.target

	[Service]
	Type=oneshot
	ExecStart=/usr/local/sbin/ipreset-configs

	[Install]
	WantedBy=network.target

Run the following command to enable the 'ipreset' service that was created:

	systemctl enable ipreset.service

## Enable SystemD NetworkD so it runs on the next boot.

	systemctl enable systemd-networkd

## SystemD NetworkD configuration files

Thank you to thequailman on GitHub for their [example on configuring VLANs on bridges](https://github.com/systemd/systemd/issues/8087#issuecomment-378714138); without this I would not have figured it out as the manuals are quite unclear.

Create the following files in /etc/systemd/network/:

### br0.netdev

This creates the bridge 'br0' which is where all the traffic on the router will pass through. The VLANFiltering option allows the bridge to process tagged VLAN packets.

	[NetDev]
	Name=br0
	Kind=bridge

	[Bridge]
	VLANFiltering=yes

### br0.network

This attaches the mgt, wan and lan VLAN devices (specified in 'mgt.netdev', 'wan.netdev' and 'lan.netdev' respectively) to the 'br0' bridge. The '[BridgeVLAN]' section allows VLANs 1, 2 and 3 on the bridge.

	[Match]
	Name=br0

	[Network]
	VLAN=mgt
	VLAN=wan
	VLAN=lan

	[BridgeVLAN]
	VLAN=1

	[BridgeVLAN]
	VLAN=2

	[BridgeVLAN]
	VLAN=3

### ens3.network

Configure the 'ens3' network device to be on the 'br0' bridge. This interface only send out traffic from VLAN 2 on the bridge and this traffic will be sent untagged. All traffic coming in on this interface will be sent to VLAN 2 on the bridge. This is an 'access' port.

	[Match]
	Name=ens3

	[Network]
	Bridge=br0

	[BridgeVLAN]
	PVID=2
	EgressUntagged=2
	VLAN=2

### ens4.network

Configure the 'ens4' network device to be on the 'br0' bridge. If this interface receives tagged traffic for VLAN 3 it will be passed on to the bridge on that same VLAN; all other tagged VLANs are ignored. However, any traffic received by this interface which is untagged will be sent to the bridge on VLAN 1 (mgt). This interface will output traffic from VLAN 3 on the bridge as tagged and traffic from VLAN 1 on the bridge as untagged.

	[Match]
	Name=ens4

	[Network]
	Bridge=br0

	[BridgeVLAN]
	PVID=1
	VLAN=1

	[BridgeVLAN]
	VLAN=3

### ens5.network

Same as the 'ens3' interface but for VLAN 3 (lan) instead of VLAN 2.

	[Match]
	Name=ens5

	[Network]
	Bridge=br0

	[BridgeVLAN]
	PVID=3
	EgressUntagged=3
	VLAN=3

### lan.netdev

Creates the 'lan' VLAN interface. This interface will only receive traffic tagged to VLAN 3. This is a virtual interface which needs to be tied to a port or bridge, which is done in 'br0.network' in the '[Network]' section.

	[NetDev]
	Name=lan
	Kind=vlan

	[VLAN]
	Id=3

### lan.network

Set the IP address for the 'lan' interface and enable the DHCP server. The DHCP server will distribute 8.8.8.8 as the DNS server and (by default) also provide its own IP address as the default gateway.

	[Match]
	Name=lan

	[Network]
	Address=10.0.2.1/24
	DHCPServer=yes

	[DHCPServer]
	EmitDNS=yes
	DNS=8.8.8.8

### mgt.netdev

Same as 'lan.netdev' but the interface is named 'mgt' instead of 'lan' and the VLAN is 1 instead of 3.

	[NetDev]
	Name=mgt
	Kind=vlan

	[VLAN]
	Id=1

### mgt.network

Same as 'lan.network' but for the 'mgt' interface and using a different statically-assigned IP address.

	[Match]
	Name=mgt

	[Network]
	Address=10.0.1.1/24
	DHCPServer=yes

	[DHCPServer]
	EmitDNS=yes
	DNS=8.8.8.8

### wan.netdev

Same as 'lan.netdev' but the interface is named 'wan' instead of 'lan' and the VLAN is 2 instead of 3.

	[NetDev]
	Name=wan
	Kind=vlan

	[VLAN]
	Id=2

### wan.network

Use DHCP to get the IP address for the 'wan' interface. This is a DHCP client as opposed to the 'lan' and 'mgt' interfaces which are DHCP servers. Also enable IP forwarding in the kernel so that traffic can be routed between subnets. This setting is enabled across the kernel; not just for this interface.

	[Match]
	Name=wan

	[Network]
	DHCP=yes
	IPForward=yes

## Reboot

Reboot the machine. Once done the machine should now be acting a a router.

* ens3 is an access port on VLAN 2. This is the WAN.
* ens4 is a hybrid port with a native (untagged) VLAN of 1 and tagged to VLAN 3.
* ens5 is an access port on VLAN 3.
