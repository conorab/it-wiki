# Useful Fortigate Commands

## View Routing Logic and Flow

The following will show all routing information for traffic with the given IP address for 100 packets.

	diag debug flow filter add <IP address to view in flow>
	diag debug flow show
	diag debug flow trace start 100
	diag debug enable

This is from: https://kb.fortinet.com/kb/viewContent.do?externalId=FD30038

## Disable Reverse Path Forwarding Protection

This protection is to prevent you from responding to traffic from addresses that should never come over that interface. If traffic originates from an interface and has a source address that is not routable over that interface then FortiOS RPF (Reverse Path Forwarding) will refuse it.

This can cause issues for VPNs where you are trying to access a network that is routed through the VPN but not in the same subnet as the VPN interface.

The following disables/weakens this, but you might be able to fix this with a better routing configuration.

	config system settings
	     set asymroute enable
	end

A much better (and probably more accurate) explanation of this can be found at: https://kb.fortinet.com/kb/documentLink.do?externalID=FD30543
