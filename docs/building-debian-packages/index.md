# Building Debian Packages

* [4. Source packages](https://www.debian.org/doc/debian-policy/ch-source.html)
* [Chapter 4. Required files under the debian directory](https://www.debian.org/doc/manuals/maint-guide/dreq.en.html)
* [howto: uploading to people.d.o using dput](https://upsilon.cc/~zack/blog/posts/2009/04/howto:_uploading_to_people.d.o_using_dput/)

## Import Note

This article was originally written on 2020-01-06 UTC+11 and moved on 2020-10-05 18:58Z+11 to [Conor Buckley's Wiki](https://www.conorab.com/wiki/).
It was then moved to here on 2021-01-03 19:15Z+11.
