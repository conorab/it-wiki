# Verifying an 'OpenSSL' Certificate Against a C.A. Certificate

In general...

This command is known to work on Debian Jessie using OpenSSL. Your experience with other versions of OpenSSL may vary.

	openssl verify -CAfile <path to C.A. file> <path to certificate to verify>

The command above is fairly self-explanatory; replace '<path to C.A. file>' with the path to the certificate which has signed the certificate you are verifying, and replace '<path to certificate to verify>' with the path to the certificate you wish to verify is signed. If '<path to certificate to verify>' has been signed by '<path to C.A. file>', you should see the command return something similar to the following:

	<path to certificate to verify>: OK

Revoked Certificates

In some cases an S.S.L. certificate may be revoked by the issuer; due to the private key being compromised (for example). While there are mechanisms in place to notify clients that a certificate has been revoked, not all places will use them. An example of this was my personal C.A. system where I would create my own certificate authority and sign my server certificates. Because I have not set up the infrastructure to allow the revocation of these certificates to be checked, it was possible to compromise one of my server keys and certificates and continue to use it for as long as it was valid. This was partly mitigated by having every server certificate and key one be valid for 1 month to minimise the usefulness of a compromised certificate. Because of this issue, it is worth keeping in mind that a 'valid' certificate does NOT necessarily demonstrate whether or not it is being used by the authorised party.

## Import Note

This article was originally written on an 2017-02-18 UTC+11 and moved to [Conor Buckley's Wiki](https://www.conorab.com/wiki/) on 2020-10-05 18:53Z+11. It was then moved to here on 2021-01-03 20:03Z+11.
