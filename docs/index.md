# Welcome to IT Wiki

This is intended as a collaborating IT wiki for admins to share their knowledge and avoid duplicating effort.

## Licence

Unless otherwise indicated the contents of this wiki are licenced under the [Creative Commons Attribution-ShareAlike 4.0 International licence](https://creativecommons.org/licenses/by-sa/4.0/legalcode). If a page, file or other item is part of this wiki and mentions it is under a different licence then that licence will apply instead.
This wiki is created using [MkDocs](https://www.mkdocs.org) and is covered by a [seperate licence which can be found on their website](https://www.mkdocs.org/about/license/). 
This wiki also uses the ReadTheDocs theme which is also [covered by a seperate licence](https://github.com/readthedocs/sphinx_rtd_theme/blob/master/LICENSE).

