# G.P.G. Commandline Examples

## [Re]Definitions

### Program/Project Name
G.P.G. can mean [G.N.U.P.G. as in the project](https://www.gnupg.org/), or refer to the 'gpg' and similiarly named programs which are part of the G.N.U.P.G. project. 

### Versions
Unless explicitly stated otherwise, G.P.G. 1 will refer to G.P.G. 1.4.18-7+deb8u4 from Debian 8/Jessie, and G.P.G. 2 will refer to G.P.G. 2.1.18-8~deb9u1 from Debian 9/Stretch. 

### gpg.conf
While the path to the G.P.G. configuration file can be changed, 'gpg.conf' is stored at '~/.gnupg/gpg.conf' by default in Debian 8/Jessie and Debian 9/Stretch. 

### Referencing the 'gpg' Command
To try and keep things simple, commands/arguments which are intended to work with G.P.G. 1 and 2 will be provided as 'gpg <arguments\>', commands/arguments which work only with G.P.G. 1 will be provided as 'gpg1 <arguments\>', and commands/arguments which work only with G.P.G. 2 will be provided as 'gpg2 <arguments\>'. The only exception to this is text inside quote boxes. 

    This is text inside a quote box. 
    
##  Copyright/Licence 

This guide contains quotes from the Texinfo manuals in G.P.G. 1 and 2 which are not owned by Conor Andrew Buckley. For information on licensing of the quotes from these manuals, see the '[Credit and Disclaimer](#credit-and-disclaimer)' section.
All other content in this guide is owned and licensed by Conor Andrew Buckley [Copyright (C) 2016-2018] under the terms of the [G.P.L.v3](gpl-3.0.txt).

## Credit and Disclaimer

In creating the guide I have used a mix of sources: search-foo, fiddling with G.P.G., and referring to the Man and Texinfo pages. This guide contains a substantial amount of output from the G.P.G. tool(s).

## G.P.G. Manuals and Source Code 

As stated in the conditions of the [G.P.L.v3](gpl-3.0.txt) (the licence applied to the previously mentioned G.P.G. tool[s]), I have provided copies of the sources for these versions of G.P.G. below, and this guide is licensed under the same terms. Copies of G.P.G. 1 and 2 (obtained from the G.P.G. website, and not the Debian 8/Jessie and 9/Stretch source) have been provided below. The Texinfo pages I referred to earlier are included in the source tarballs of G.P.G., and the locations of these pages have also been noted below. I have compared the manuals (Man and Texinfo) provided in Debian 8/Jessie and 9/Stretch to the ones provided in these sources and there are no **significant** differences; none that would change this guide. 

### G.P.G. Source Code

* v1.4.18
    *  [Source Code Licence](gpl-3.0.txt)
    *  [Source Tarball](gnupg-1.4.18.tar.bz2)
    *  [Source Tarball Signature](gnupg-1.4.18.tar.bz2.sig)
    *    Texinfo manual locations:
        *   doc/gnupg1.info 
* v2.1.18
    * [Source Code Licence](gpl-3.0.txt)
    * [Source Tarball](gnupg-2.1.18.tar.bz2)
    * [Source Tarball Signature](gnupg-2.1.18.tar.bz2.sig)
    * Texinfo manual locations:
        * doc/gnupg.info
        * doc/gnupg.info-1
        * doc/gnupg.info-2 

##  G.P.G. Commandline Examples 

These examples assume you are using default G.P.G. options. If you have modified 'gpg.conf', then your results may vary.

This does not attempt to cover a wide variety of uses, or go in-depth into command arguments. If you would like more in-depth information, please refer to the G.P.G. manual. 

###   Signing a File 

Create a signed copy of '<file\>'. The signed copy of '<file\>' will be saved as '<file\>.gpg'. The key which will be used for signing will be the first key in your secret key ring.

    gpg --sign <file> 

###   Encrypting a File 

Encrypt '<file\>' to <the key to encrypt to\>. The encrypted copy of '<file\>' will be saved as '<file\>.gpg'.

    gpg --recipient <The key to encrypt to> --encrypt <file> 

### Sign and Encrypt a File with Specified Key

Sign '<file\>' with <the key to sign with\> and encrypt it to <the key to encrypt to\>. The signed and encrypted copy of '<file\>' will be saved as '<file\>.gpg'.

    gpg --local-user <the key to sign with> --recipient <the key to encrypt to> --sign --encrypt <file> 

###  Sign and Encrypt a File with Specified Key, Cipher and Digest 

Sign '<file\>' with <the key to sign with\> and encrypt it to <the key to encrypt to\> forcing 'AES256' as the cipher and 'SHA512' as the digest. This is useful when you know the recipient can handle the forced cipher and digest and went to ensure it will always be the same combination.

    gpg --cipher-algo AES256 --digest-algo SHA512 --local-user <the key to sign with> --recipient <the key to encrypt to> --sign --encrypt <file> 

###  Finding and Retrieving Keys from a Key Server 

The instructions between G.P.G. 1 and G.P.G. 2 vary slightly due to the amount of the key I.D. they display when searching, as well as default packages in Debian 8 and Debian 9. 

####  G.P.G. 1 with Debian 8 

Search a key server for the key you are looking for by running 'gpg --keyserver <address of key server\> --search-keys <key search term\>'. In my case, I am using the key server redirect at 'keys.gnupg.net' and searching for my key by name (Conor Andrew Buckley). 

    user@gpgref-jessie:~$ gpg --keyserver hkp://keys.gnupg.net --search-keys 'Conor Andrew Buckley'
    gpg: searching for "Conor Andrew Buckley" from hkp server keys.gnupg.net
    (1) Conor Andrew Buckley @ dragon-blood - 2017-06-18_21-28 (Key for use ex
    4096 bit RSA key 13E0B47F, created: 2017-06-18, expires: 2018-06-18
    (2) Conor Andrew Buckley
    4096 bit RSA key D648274B, created: 2017-01-17
    Keys 1-2 of 2 for "Conor Andrew Buckley". Enter number(s), N)ext, or Q)uit > 2
    
Here we are asked which key we would like to download. In my case, I would like to download the second key, and so I type '2' and press ENTER. The key is then downloaded. 

    gpg: requesting key D648274B from hkp server keys.gnupg.net
    gpg: key D648274B: public key "Conor Andrew Buckley" imported
    gpg: 3 marginal(s) needed, 1 complete(s) needed, PGP trust model
    gpg: depth: 0 valid: 3 signed: 0 trust: 0-, 0q, 0n, 0m, 0f, 3u
    gpg: Total number processed: 1
    gpg: imported: 1 (RSA: 1)
    user@gpgref-jessie:~$ 

If you were told the fingerprint of the key you were downloading, it is best to make you have downloaded a key with a matching fingerprint. If you were not told the fingerprint of the key you were downloading, you may skip the remainder of these steps. This is to ensure that you haven't downloaded a malicious key with similar details. In the previous command, we were told what the short I.D. of the key we downloaded was. In my case, the short key I.D. for the key I downloaded was 'D648274B'. We will now get the fingerprint of this key by running 'gpg --fingerprint <short key I.D.\>'. 

    user@gpgref-jessie:~$ gpg --fingerprint D648274B
    pub 4096R/D648274B 2017-01-17
    Key fingerprint = 4EBC 6605 510F BC53 604C ABEF 3912 142E D648 274B
    uid Conor Andrew Buckley
    uid [jpeg image of size 5400]
    sub 4096R/92FA7361 2017-02-11 [expires: 2018-02-11]

From the output of the previous command, we can see the key fingerprint is '4EBC 6605 510F BC53 604C ABEF 3912 142E D648 274B'. If you were told what the fingerprint of the public key was prior to downloading it (say, on Facebook or over the phone), compare that to the fingerprint returned by this command. If they are the same (don't worry about spacing or capitalisation) then you have retrieved the correct key and may skip the remainder of these steps. If not, it's possible the key was corrupted during the download or tampered with (wrong key). If so, you can delete the key you downloaded by running 'gpg --delete-key <key fingerprint from last command\>'. 

    user@gpgref-jessie:~$ gpg --delete-key '4EBC 6605 510F BC53 604C ABEF 3912 142E D648 274B'
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    pub 4096R/D648274B 2017-01-17 Conor Andrew Buckley

    Delete this key from the keyring? (y/N) y
    
Once done, you may repeat the first two steps to re-retrieve the key. If the fingerprint still doesn't match, delete the key again and let those who gave you the fingerprint know that you are unable to retrieve their key. 

#### G.P.G. 2 on Debian 9 

Before starting, you may need to install 'dirmngr' as this is not part of the default 'GNOME Desktop' installation.

Search a key server for the key you are looking for by running 'gpg --keyserver <address of key server\> --search-keys <key search term\>'. In my case, I am using the key server redirect at 'keys.gnupg.net' and searching for my key by name (Conor Andrew Buckley). 

    user@gpgref-stretch:~$ gpg --keyserver hkp://keys.gnupg.net --search-keys 'Conor Andrew Buckley'
    gpg: data source: http://51.15.0.17:11371
    (1) Conor Andrew Buckley @ dragon-blood - 2017-06-18_21-28 (Key for use ex
    4096 bit RSA key CD56B54713E0B47F, created: 2017-06-18, expires: 2018-06-18
    (2) Conor Andrew Buckley
    4096 bit RSA key 3912142ED648274B, created: 2017-01-17
    Keys 1-2 of 2 for "Conor Andrew Buckley". Enter number(s), N)ext, or Q)uit > 2

Here we are asked which key we would like to download. In my case, I would like to download the second key, and so I type '2' and press ENTER. The key is then downloaded. 

    gpg: key 3912142ED648274B: public key "Conor Andrew Buckley" imported
    gpg: marginals needed: 3 completes needed: 1 trust model: pgp
    gpg: depth: 0 valid: 3 signed: 0 trust: 0-, 0q, 0n, 0m, 0f, 3u
    gpg: Total number processed: 1
    gpg: imported: 1
    
If you were told the fingerprint of the key you were downloading, it is best to make you have downloaded a key with a matching fingerprint. If you were not told the fingerprint of the key you were downloading, you may skip the remainder of these steps. This is to ensure that you haven't downloaded a malicious key with similar details. In the previous command, we were told what the key I.D. of the key we downloaded was. In my case, the key I.D. for the key I downloaded was '3912142ED648274B'. We will now get the fingerprint of this key by running 'gpg --fingerprint <key I.D.\>'. 

    user@gpgref-stretch:~$ gpg --fingerprint 3912142ED648274B
    pub rsa4096 2017-01-17 [SC]
    4EBC 6605 510F BC53 604C ABEF 3912 142E D648 274B
    uid [ unknown] Conor Andrew Buckley
    uid [ unknown] [jpeg image of size 5400]
    sub rsa4096 2017-02-11 [SE] [expires: 2018-02-11]

From the output of the previous command, we can see the key fingerprint is '4EBC 6605 510F BC53 604C ABEF 3912 142E D648 274B'. If you were told what the fingerprint of the public key was prior to downloading it (say, on Facebook or over the phone), compare that to the fingerprint returned by this command. If they are the same (don't worry about spacing or capitalisation) then you have retrieved the correct key and may skip the remainder of these steps. If not, it's possible the key was corrupted during the download or tampered with (wrong key). If so, you can delete the key you downloaded by running 'gpg --delete-key <key fingerprint from last command\>'. 

    user@gpgref-stretch:~$ gpg --delete-key '4EBC 6605 510F BC53 604C ABEF 3912 142E D648 274B'
    gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    pub rsa4096/3912142ED648274B 2017-01-17 Conor Andrew Buckley

    Delete this key from the keyring? (y/N) y

Once done, you may repeat the first two steps to re-retrieve the key. If the fingerprint still doesn't match, delete the key again and let those who gave you the fingerprint know that you are unable to retrieve their key. 

###  Generating a G.P.G. Key

The instructions between G.P.G. 1 and G.P.G. 2 vary slightly due to the amount of the key I.D. they display when searching, as well as default packages in Debian 8 and Debian 9. 

#### G.P.G. 1 on Debian 8

Start the process by running 'gpg --gen-key'. You will be asked what key types to use for your key and for the key size. Select 'RSA and RSA' for the key type and '2048' for the key size. 

    user@gpgref-jessie:~$ gpg --gen-key
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Please select what kind of key you want:
    (1) RSA and RSA (default)
    (2) DSA and Elgamal
    (3) DSA (sign only)
    (4) RSA (sign only)
    Your selection? 1
    RSA keys may be between 1024 and 4096 bits long.
    What keysize do you want? (2048) 2048
    Requested keysize is 2048 bits

You will now be asked how long the key should last. We will be setting this to 1 year, meaning the key will not be recognised as valid 1 year from now. Note that the date can be extended later on; keeping the validity length low means that if you abandon or lose access to the key, people will not trust it for too long. 

    Please specify how long the key should be valid.
    0 = key does not expire
    <n> = key expires in n days
    <n>> = key expires in n weeks
    <n>m = key expires in n months
    <n>y = key expires in n years
    Key is valid for? (0) 1y
    Key expires at Mon 14 Jan 2019 11:50:43 AEDT
    Is this correct? (y/N) y

You will now be asked for a 'Real Name', 'Email address' and 'Comment'. The 'Real Name' is the only field here which is required. Note that when somebody signs your key they are potentially asserting that any of the details provided here (name, e-mail address, comment) are correct. Because of this, avoid providing any disputable details such as putting 'Greatest baker in the world' in your comment; stick to objective information.

Lastly, you will be asked to confirm that these details are correct. If not, you can press 'n', 'c' or 'e' to change the name, e-mails address or comment. Otherwise, press 'o' to continue. 

    You need a user ID to identify your key; the software constructs the user ID
    from the Real Name, Comment and Email Address in this form:
    "Heinrich Heine (Der Dichter) "

    Real name: Conor Andrew Buckley
    Email address: me@conorab.com
    Comment:
    You selected this USER-ID:
    "Conor Andrew Buckley "

    Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? o

You will be asked to enter a passphrase. Enter a strong passphrase or enter nothing to leave your key without a passphrase. You should only ever refuse having a password if you plan to use the key in automation (scripting, for example).

You may then be notified that there is not enough entropy to generate the key. One method of increasing entropy is to open a blank text document and type gibberish, as your key presses are used as one of the sources of entropy. If you decide to generate entropy this way, make sure you don't save the document. Avoid pressing keys in a predictable way such as typing 'qwerty', names, non-random numbers, sentences, etc. 

    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.

    Not enough random bytes available. Please do some other work to give
    the OS a chance to collect more entropy! (Need 279 more bytes)
    ..+++++
    ..+++++
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.

    Not enough random bytes available. Please do some other work to give
    the OS a chance to collect more entropy! (Need 92 more bytes)
    ......+++++

    Not enough random bytes available. Please do some other work to give
    the OS a chance to collect more entropy! (Need 74 more bytes)
    .+++++

Once enough entropy is available, your key will be generated and basic information about it will be displayed. You key has now been generated. 

    gpg: key 92372E5D marked as ultimately trusted
    public and secret key created and signed.

    gpg: checking the trustdb
    gpg: 3 marginal(s) needed, 1 complete(s) needed, PGP trust model
    gpg: depth: 0 valid: 1 signed: 0 trust: 0-, 0q, 0n, 0m, 0f, 1u
    gpg: next trustdb check due at 2019-01-14
    pub 2048R/92372E5D 2018-01-14 [expires: 2019-01-14]
    Key fingerprint = 39E2 30FD 9D39 2DBE 6B0C F23A 61BF B923 9237 2E5D
    uid Conor Andrew Buckley <me@conorab.com>
    sub 2048R/4EF17E37 2018-01-14 [expires: 2019-01-14]

#### G.P.G. 2 on Debian 9 

Start the process by running 'gpg --full-gen-key'. You will be asked what key types to use for your key and for the key size. Select 'RSA and RSA' for the key type and '2048' for the key size. 

    user@gpgref-stretch:~$ gpg --full-gen-key
    gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Please select what kind of key you want:
    (1) RSA and RSA (default)
    (2) DSA and Elgamal
    (3) DSA (sign only)
    (4) RSA (sign only)
    Your selection? 1
    RSA keys may be between 1024 and 4096 bits long.
    What keysize do you want? (2048) 2048
    Requested keysize is 2048 bits

You will now be asked how long the key should last. We will be setting this to 1 year, meaning the key will not be recognised as valid 1 year from now. Note that the date can be extended later on; keeping the validity length low means that if you abandon or lose access to the key, people will not trust it for too long. 

    Please specify how long the key should be valid.
    0 = key does not expire
    <n> = key expires in n days
    <n>w = key expires in n weeks
    <n>m = key expires in n months
    <n>y = key expires in n years
    Key is valid for? (0) 1y
    Key expires at Mon 14 Jan 2019 12:55:09 AEDT
    Is this correct? (y/N) y

You will now be asked for a 'Real name', 'Email address' and 'Comment'. The 'Real Name' is the only field here which is required. Note that when somebody signs your key they are potentially asserting that any of the details provided here (name, e-mail address, comment) are correct. Because of this, avoid providing any disputable details such as putting 'Greatest baker in the world' in your comment; stick to objective information.

Lastly, you will be asked to confirm that these details are correct. If not, you can press 'n', 'c' or 'e' to change the name, e-mails address or comment. Otherwise, press 'o' to continue. 

    GnuPG needs to construct a user ID to identify your key.

    Real name: Conor Andrew Buckley
    Email address: me@conorab.com
    Comment:
    You selected this USER-ID:
    "Conor Andrew Buckley <me@conorab.com>"

    Change (N)ame, (C)omment, (E)mail or (O)kay/(Q)uit? o

You will be asked to enter a passphrase. Enter a strong passphrase or enter nothing to leave your key without a passphrase. You should only ever refuse having a password if you plan to use the key in automation (scripting, for example).

You may then be notified that there is not enough entropy to generate the key. One method of increasing entropy is to open a blank text document and type gibberish, as your key presses are used as one of the sources of entropy. If you decide to generate entropy this way, make sure you don't save the document. Avoid pressing keys in a predictable way such as typing 'qwerty', names, non-random numbers, sentences, etc. 

    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.
    We need to generate a lot of random bytes. It is a good idea to perform
    some other action (type on the keyboard, move the mouse, utilize the
    disks) during the prime generation; this gives the random number
    generator a better chance to gain enough entropy.

Once enough entropy is available, your key will be generated and basic information about it will be displayed. You key has now been generated. 

    gpg: key 3FE484DCB5BA2C44 marked as ultimately trusted
    gpg: directory '/home/user/.gnupg/openpgp-revocs.d' created
    gpg: revocation certificate stored as '/home/user/.gnupg/openpgp-revocs.d/75CCCC0A4C6E1125CFA383BF3FE484DCB5BA2C44.rev'
    public and secret key created and signed.

    pub rsa2048 2018-01-14 [SC] [expires: 2019-01-14]
    75CCCC0A4C6E1125CFA383BF3FE484DCB5BA2C44
    75CCCC0A4C6E1125CFA383BF3FE484DCB5BA2C44
    uid Conor Andrew Buckley <me@conorab.com>
    sub rsa2048 2018-01-14 [E] [expires: 2019-01-14]

###  Sending a Public Key to a Key Server

The instructions for this vary slightly between Debian 8 with G.P.G. 1 and Debian 9 with G.P.G. 2 due to differing packages and displayed key I.D.s. 

####  Debian 8 with G.P.G. 1

Determine the key I.D. of the key you wish to upload to a key server by running 'gpg --list-keys'. 

    user@gpgref-jessie:~$ gpg --list-keys
    /home/user/.gnupg/pubring.gpg
    -----------------------------
    pub 4096R/D648274B 2017-01-17
    uid Conor Andrew Buckley
    uid [jpeg image of size 5400]
    sub 4096R/92FA7361 2017-02-11 [expires: 2018-02-11]
    
In the example above, the I.D. of our public key is 'D648274B'. Now, upload the corresponding public key to a key server using the following: 'gpg --keyserver <key server address\> --send-keys <key I.D.\>'. 

    user@gpgref-jessie:~$ gpg --keyserver hkp://keys.gnupg.net --send-keys D648274B
    gpg: sending key D648274B to hkp server keys.gnupg.net

In the example above, we have used the G.P.G. key server address ('hkp://keys.gnupg.net'). Your key has now been uploaded to the key server. 

#### Debian 9 with G.P.G. 2 

Before starting, you may need to install 'dirmngr' as this is not part of the default 'GNOME Desktop' installation.

Determine the key I.D. of the key you wish to upload to a key server by running 'gpg --list-keys'. 

    user@gpgref-stretch:~$ gpg --list-keys
    /home/user/.gnupg/pubring.kbx
    -----------------------------
    pub rsa4096 2017-01-17 [SC]
    4EBC6605510FBC53604CABEF3912142ED648274B
    uid [ unknown] Conor Andrew Buckley
    uid [ unknown] [jpeg image of size 5400]
    sub rsa4096 2017-02-11 [SE] [expires: 2018-02-11]
    
In the example above, the I.D. of our public key is '4EBC6605510FBC53604CABEF3912142ED648274B'. Now, upload the corresponding public key to a key server using the following: 'gpg --keyserver <key server address\> --send-keys <key I.D.\>'. 

    user@gpgref-stretch:~$ gpg --keyserver hkp://keys.gnupg.net --send-keys 4EBC6605510FBC53604CABEF3912142ED648274B
    gpg: sending key 3912142ED648274B to hkp://keys.gnupg.net

In the example above, we have used the G.P.G. key server address ('hkp://keys.gnupg.net'). Your key has now been uploaded to the key server. 
