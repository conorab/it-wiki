# Setting Up AWS Gaming Machine

## Credits

Some of the steps and inspiration for this article is taken from a guide on [Medium](https://medium.com/@bmatcuk/gaming-on-amazon-s-ec2-83b178f47a34) by Bob Matcuk.
Some of this information no longer works or does not apply to the type of EC2 instance mentioned here but never-the-less it was crucial in building this guide.

## Guide

Unless otherwise specified, run any code snippets in the same Powershell window.

## Change the Administrator password and enable automatic login

This is so Steam can launch automatically and remote access can be done through that instead.
Logging via RDP causes issues here, so once this is all setup, you should avoid logging in with RDP at all and reboot if you ever do.

	$serviceAccount='Administrator'
	$servicePassword='<your new administrator password>'

	$servicePassword_secureString=ConvertTo-SecureString "$servicePassword" -AsPlaintext -Force
	$serviceCredentials=New-Object -typename System.Management.Automation.PSCredential -ArgumentList $serviceAccount, $servicePassword_secureString

	$UserAccount = Get-LocalUser -Name $serviceAccount
	$UserAccount | Set-LocalUser -Password $servicePassword_secureString

### Enable automatic login 

The following code to enable automatic login is adapted from [an article on sysadmins.eu](http://www.sysadmins.eu/2014/12/set-up-auto-logon-in-windows-using.html):

	New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon' -Name AutoAdminLogon -Value 1 -ErrorAction SilentlyContinue
	New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon' -Name DefaultUserName -Value $serviceAccount -ErrorAction SilentlyContinue
	New-ItemProperty -Path 'HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon' -Name DefaultPassword -Value $servicePassword -ErrorAction SilentlyContinue

## Install the NVIDIA driver.

This will download the NVIDIA drivers to your Downloads folder in the NVIDIA sub-folder. There will be two executables. Run the one which matches your OS and install the NVIDIA driver. Do not reboot yet.

The Powershell code used in this section comes from a [piece of AWS documentatio](https://docs.aws.amazon.com/AWSEC2/latest/WindowsGuide/install-nvidia-driver.html#nvidia-gaming-driver):

### Create IAM Policy Only Allowing Access to NVIDIA S3 Bucket

1. Go to [IAM Policies](https://console.aws.amazon.com/iam/home#/policies).
1. Click "Create Policy".
1. Click on the "JSON" tab.
1. Delete anything in the text box and paste the following: 

		{
		    "Version": "2012-10-17",
		    "Statement": [
			{
			    "Sid": "VisualEditor0",
			    "Effect": "Allow",
			    "Action": [
				"s3:DescribeJob",
				"s3:Get*",
				"s3:List*"
			    ],
			    "Resource": [
				"arn:aws:s3:us-east-1:*:accesspoint/*",
				"arn:aws:s3:*:070314627337:job/*",
				"arn:aws:s3:::*/*",
				"arn:aws:s3:*:070314627337:storage-lens/*",
				"arn:aws:s3:::nvidia-gaming"
			    ]
			}
		    ]
		}

5. Click "Review policy".
6. In the "Name" field, enter: "S3ReadNVIDIABucket".
7. Click "Create policy".
8. On the left side-bar, click "Users".
9. Click "Add user".
10. In the "User name" field, enter "aws-game1".
11. Under "Access type" tick "Programmatic access".
12. Click "Next: permissions".
13. Click "Attach existing policies directly".
14. In the search box enter: "S3ReadNVIDIABucket".
15. Tick the box next to "S3ReadNVIDIABucket" in the list below.
16. Click "Next: Tags".
17. Click "Next: Review".
18. Click "Create user".
19. Note down the "Access key ID".
20. Under "Secret access key" click "Show".
21. Copy the text under "Secret access key" (except for "Hide" at the end).
22. Click "Close".

### Add your AMI creds to the local config on the machine.

This is so the command below can download from the public S3 bucket.
Replace "<your access key>" and "<your secret key>" with the "Access key ID" and "Secret access key" noted earlier.

	Set-AWSCredential `
			  -AccessKey <your access key> `
			  -SecretKey <your secret key> `
			  -StoreAs default

### Download the NVIDIA Driver

	$Bucket = "nvidia-gaming"
	$KeyPrefix = "windows/latest"
	$LocalPath = "$home\Downloads\NVIDIA"
	$Objects = Get-S3Object -BucketName $Bucket -KeyPrefix $KeyPrefix -Region us-east-1
	foreach ($Object in $Objects) {
	    $LocalFileName = $Object.Key
	    if ($LocalFileName -ne '' -and $Object.Size -ne 0) {
		$LocalFilePath = Join-Path $LocalPath $LocalFileName
		Copy-S3Object -BucketName $Bucket -Key $Object.Key -LocalFile $LocalFilePath -Region us-east-1
	    }
	}

## Download and install Steam:

The commands below will download the Steam installer from their website and run it. Install Steam following the instructions in the wizard.

	Invoke-WebRequest -Uri "https://cdn.cloudflare.steamstatic.com/client/installer/SteamSetup.exe" -OutFile "$home\Downloads\SteamSetup.exe"
	. "$home\Downloads\SteamSetup.exe"

## Download and install Virtual Audio Cable

Similar to above: this will download Virtual Audio Cable and run the installer.
Follow the prompts to install Virtual Audio Cable.
This is needed since the EC2 instance doesn't have an audio device and so games won't have sound.
This software creates a loopback audio device, which is all we need.
This software doesn't cost anything but it's great software. 
[Give them some money](https://vb-audio.com/Services/licensing.htm)

	Invoke-WebRequest -Uri "https://download.vb-audio.com/Download_CABLE/VBCABLE_Driver_Pack43.zip" -OutFile "$home\Downloads\VBCABLE_Driver_Pack.zip"
	mkdir "$home\Downloads\VBCABLE_Driver_Pack"
	Expand-Archive -LiteralPath "$home\Downloads\VBCABLE_Driver_Pack.zip" -DestinationPath "$home\Downloads\VBCABLE_Driver_Pack"
	. "$home\Downloads\VBCABLE_Driver_Pack\VBCABLE_Setup_x64.exe"

## Enable the Windows Audio Service

	Set-Service -Name Audiosrv -StartupType Automatic

## Reboot

Reboot your machine to finish installing the Virtual Audio Cable and NVIDIA drivers.

### Install the NVIDIA GPU licence
The two following commands are needed to licence the GPU. Without this your resolution will be limited to 1366x768.

	New-ItemProperty -Path "HKLM:\SOFTWARE\NVIDIA Corporation\Global" -Name "vGamingMarketplace" -PropertyType "DWord" -Value "2"

Download the certificate used to validate the licence.

	Invoke-WebRequest -Uri "https://nvidia-gaming.s3.amazonaws.com/GridSwCert-Archive/GridSwCert-Windows_2020_04.cert" -OutFile "$Env:PUBLIC\Documents\GridSwCert.txt"

## Reboot

Reboot again for the NVIDIA licence to apply.

## Download the "Remote Desktop" program from GitHub.

The following command will download the Remote Desktop application from GitHub. This is different to Microsoft's Remote Desktop. All this application does is allow you to access your desktop through Steam RemotePlay. The repo can be found at "https://github.com/tyami94/RemoteDesktop/releases"

	Invoke-WebRequest -Uri "https://github.com/tyami94/RemoteDesktop/releases/download/v1.0.0/RemoteDesktop.exe" -OutFile "$home\Desktop\Remote Desktop.exe"

## Configure Steam

Sign in to Steam and set it to remember your password. Your password needs to be remembered so you don't need to RDP to the machine to start Steam and get better remote access.

1. Open Steam.
1. Click "Steam", then "Settings".
1. Click on the "Remote Play" tab.
1. Make sure "Enable Remote Play" is ticked.
1. Make sure "Allow Direct IP Connection (IP Sharing)" is set to "My Devices" at least.
1. Click "Advanced Host Options".
1. Make sure the following checkboxes are as follows:
	* Untick - Play audio on host
	* Tick - Change desktop resolution to match streaming client.
	* Tick - Dynamically adjust capture resolution to improve performance.
	* Tick - Use NVFBC capture on NVIDIA GPU.
	* Tick - Enable hardware encoding.
	* Tick - Enable hardware encoding on NVIDIA GPU.
1. Set "Number of software encoding threads" to "Automatic".
1. Click "OK".
1. Click "OK" on the "Settings" window.
1. In the Steam window, click "ADD GAME". 
1. Click "Add a Non-Steam Game...".
1. Click "Browse".
1. Browse to your Desktop and double-click "Remote Desktop.exe".
1. Click "ADD SELECTED PROGRAMS".

## Clean up.

Here we delete all the files we downloaded for this guide that we no longer need.

	Remove-Item -Recurse "$home\Downloads\VBCABLE_Driver_Pack.zip"
	Remove-Item -Recurse "$home\Downloads\VBCABLE_Driver_Pack"
	Remove-Item -Recurse "$home\Downloads\SteamSetup.exe"
	Remove-Item -Recurse "$home\Downloads\NVIDIA"

## Disable Microsoft Basic Display Adapter.

The following Powershell command will disable all devices with the name "Microsoft Basic Display Adapter". This will generate some errors due but this is to be expected. This command is necessary to ensure that games are forced to use the NVIDIA GPU and not the emulated GPU.

	Get-PnpDevice -FriendlyName "Microsoft Basic Display Adapter" | ForEach-Object { Disable-PnpDevice -Confirm:$false -InstanceId $_.InstanceId }	

## Reboot
Reboot again to fix the RDP GPU issues.
Once the EC2 instance reboots DO NOT use RDP.
Wait for the instance to come back up and use the "Remote Desktop" non-Steam game in Steam to access your desktop.
This prevents RDP from breaking the login session.
If you must use RDP, reboot or shut down the instance when you're finished to keep everything running smoothly.
