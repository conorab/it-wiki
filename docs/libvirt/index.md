# libvirt

## Useful Commands

### Setup Basic KVM VM

CPU model is generic 'kvm64', the VM name is 'bob', it has 512 MB of RAM, 1 CPU, boots from '/var/lib/libvirt/images/ISOs/debian.iso' and set to boot from the ISCSI HDD as the secondary, RAW disk location is '/var/lib/libvirt/images/bob.img', network is 'default' with a virtio adapter and VNC with QXL is used for the display.

	virt-install \
	--connect qemu:///system \
	--cpu kvm64 \
	--name bob \
	--memory 512 \
	--vcpus 1 \
	-c /var/lib/libvirt/images/ISOs/debian.iso \
	--os-variant debian10 \
	--boot cdrom,hd \
	--disk path=/var/lib/libvirt/images/bob.img,device=disk,format=raw,bus=scsi \
	--network model=virtio,mac=RANDOM,network=default \
	--virt-type kvm \
	--memballoon virtio \
	--graphics type=vnc \
	--video qxl
