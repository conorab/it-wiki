# My gpg.conf

## [Re]Definitions

### Program/Project Name
G.P.G. can mean [G.N.U.P.G. as in the project](https://www.gnupg.org/), or refer to the 'gpg' and similiarly named programs which are part of the G.N.U.P.G. project. 

### Versions
Unless explicitly stated otherwise, G.P.G. 1 will refer to G.P.G. 1.4.18-7+deb8u4 from Debian 8/Jessie, and G.P.G. 2 will refer to G.P.G. 2.1.18-8~deb9u1 from Debian 9/Stretch. 

### gpg.conf
While the path to the G.P.G. configuration file can be changed, 'gpg.conf' is stored at '~/.gnupg/gpg.conf' by default in Debian 8/Jessie and Debian 9/Stretch. 

### Referencing the 'gpg' Command
To try and keep things simple, commands/arguments which are intended to work with G.P.G. 1 and 2 will be provided as 'gpg <arguments\>', commands/arguments which work only with G.P.G. 1 will be provided as 'gpg1 <arguments\>', and commands/arguments which work only with G.P.G. 2 will be provided as 'gpg2 <arguments\>'. The only exception to this is text inside quote boxes. 

    This is text inside a quote box. 
    
##  Copyright/Licence 

This guide contains quotes from the Texinfo manuals in G.P.G. 1 and 2 which are not owned by Conor Andrew Buckley. For information on licensing of the quotes from these manuals, see the '[Credit and Disclaimer](#credit-and-disclaimer)' section.
All other content in this guide is owned and licensed by Conor Andrew Buckley [Copyright (C) 2016-2018] under the terms of the [G.P.L.v3](gpl-3.0.txt).

## Credit and Disclaimer

In creating the guide I have used a mix of sources: search-foo, fiddling with G.P.G., and referring to the Man and Texinfo pages. In this guide, I use a mix of quotes from the Texinfo manuals, and my own comments.

## G.P.G. Manuals and Source Code 

As stated in the conditions of the [G.P.L.v3](gpl-3.0.txt) (the licence applied to the previously mentioned Man pages and Texinfo manuals), I have provided copies of the sources for these versions of G.P.G. below, and this guide is licensed under the same terms. Copies of G.P.G. 1 and 2 (obtained from the G.P.G. website, and not the Debian 8/Jessie and 9/Stretch source) have been provided below. The Texinfo pages I referred to earlier are included in the source tarballs of G.P.G., and the locations of these pages have also been noted below. I have compared the manuals (Man and Texinfo) provided in Debian 8/Jessie and 9/Stretch to the ones provided in these sources and there are no **significant** differences; none that would change this guide. 

### G.P.G. Source Code

* v1.4.18
    *  [Source Code Licence](gpl-3.0.txt)
    *  [Source Tarball](gnupg-1.4.18.tar.bz2)
    *  [Source Tarball Signature](gnupg-1.4.18.tar.bz2.sig)
    *    Texinfo manual locations:
        *   doc/gnupg1.info 
* v2.1.18
    * [Source Code Licence](gpl-3.0.txt)
    * [Source Tarball](gnupg-2.1.18.tar.bz2)
    * [Source Tarball Signature](gnupg-2.1.18.tar.bz2.sig)
    * Texinfo manual locations:
        * doc/gnupg.info
        * doc/gnupg.info-1
        * doc/gnupg.info-2 

## My gpg.conf 

To make G.P.G. faster and easier to use, you are able to save some command-line options for G.P.G. in the 'gpg.conf' file. The layout of this file is quite simple. If you would normally pass an argument to G.P.G. as follows: 'gpg --local-user <key1\>', the same option in 'gpg.conf' would be: 'local-user <key1\>' on its own line.

Below, I have included the options I use in my 'gpg.conf' file with explanations of what each line does. 

###  no-greeting 

Some commands such as 'gpg --version' will still print this notice.

From the G.P.G. v2.1.18 Texinfo page:

    Suppress the initial copyright message. 

###  local-user <key\>

The key(s) to be used for signing. If this option is specified multiple times with different keys when signing (example: 'gpg --local-user <key1\> --local-user <key2\> --sign <file to sign\>') then all specified keys will be used. [If multiple keys are specified for signing (as shown before) and one but not all specified keys are unavailable (wrong password, key doesn't exist), then the unavailable keys will be skipped for signing and signing will continue with the remaining working keys. If all specified keys are unavailable, signing will not proceed.](#signing-with-local-user-fails-when-all-specified-keys-are-unavailable)

This option is useful if you almost always sign using the same key(s). However, if you occansionally want to use a different key, then this configuration option must be removed/commented out of 'gpg.conf', as [using multiple instances of 'local-user' results in all keys specified in each instance being used, not just the most recent one.](#local-user-appends-not-replaces) The key specified can refer to the comment (unpredictable in some cases if multiple keys are associated with one U.I.D.), the short key I.D. and the long key I.D. 

### default-recipient-self

If a recipient is not given (specified by '-r <key\>' or '--recipient <key\>'), use either the first key in the secret keyring or the key given by '--default-key <key\>' as the recipient. If the 'default-key' option is given it takes priority over the first key in the secret keyring. 

### ask-cert-level

Always ask the user which level of trust they would like to apply when creating a 'trust' signature. A 'trust' signature is used to indicate the amount of work that was put into verifying the key being signed. Note that setting this option does not mean that every signature you make will be a 'trust' signature; that is what 'tsign' is for. For more information on the levels of trust which can be applied to signed key, check the [Texinfo insert in the 'default-cert-level' section.](#default-cert-level-integer)

### list-options <option(s)\> 

Allows you to override the defaults used when using the --list-keys or a similar argument is used. I have employed the following options:

* show-uid-validity - Displays whether or not the given U.I.D. is valid; I.E. if it has expired, been revoked, etc.
* show-unusable-uids - Display U.I.D.s which would otherwise not be usable; for example, a U.I.D. may have been revoked or had expired.
* show-unusable-subkeys - Same as above, but for subkeys.
* show-sig-expire - Display the expiry date of all signatures of the displayed keys, U.I.D.s, etc.

### min-cert-level <integer\>

Do not consider keys with a 'trust' level below this number as valid unless the 'trust' level is 0. For more information on the levels of trust which can be applied to signed key, check the [Texinfo insert in the 'default-cert-level' section.](#default-cert-level-integer)

This option is useful if you frequently work with people who are difficult to verify. For example, if Alice and Bob knew each other exclusively online and communicated exclusively through video chat and a forum, Alice could be reasonably confident that Bob is who he says he is, and visa-versa. But, because they haven't checked each other against a mutually trusted source such as their drivers licences using a secure channel, it's possible that they're lying about who they are and/or a man-in-the-middle attack is taking place. Because of this, if they were to sign each others keys, a trust signature with a level of 1 would be appropriate. However, by default G.P.G. only trusts keys with a certification level of 2 and above, so if somebody who knew Bob in person (let's call her Emily) wanted to communicate with Alice and rely on Bob's signature of Alice, this signature would not be trusted as it is below the default of 2. To resolve this, Emily could pass '--min-cert-level 1' as an argument to G.P.G. when working with Alice's key. This would be the best approach if Emily usually expects keys with a certification level of 2 or higher to be 'trusted' since she doesn't need to check the certification level of every signature whenever her keychain is modified to make sure they meet her 'trust' requirement of 2 or above. However, if Emily usually communicates with keys that have a certification level of 1 or lower, then setting 'min-cert-level 1' would make more sense as she doesn't need to worry about lower-than-expected certification levels being accepted (she already expects the lowest non-zero level) and doesn't need to add '--min-cert-level 1' to the command-line options of G.P.G. whenever she works with keys of lower-than-default certification levels. 

###  personal-cipher-preferences <option(s)\> 

As the default ciphers are weak, I recommend specifying 'aes256' as your ciphers.

From the v2.1.18 Texinfo page:

    Set the list of personal cipher preferences to 'string'. Use 'gpg
    --version' to get a list of available algorithms, and use 'none' to
    set no preference at all. This allows the user to safely override
    the algorithm chosen by the recipient key preferences, as GPG will
    only select an algorithm that is usable by all recipients. The
    most highly ranked cipher in this list is also used for the
    '--symmetric' encryption command. 

### personal-digest-preferences <one-or-more-strings\>

Same as above, but for digests instead of ciphers. Run 'gpg --version' to find out which digests your G.P.G. version supports. As the default digests are anywhere from horribly weak (MD5) to fairly very weak (SHA-1 - recently compromised), I recommend specifying 'sha256' as your digest.

### cipher-algo <single-string\>

Unlike 'personal-cipher-preferences', this will force the cipher which you use, meaning if the recipient's preferences prefer to use a cipher you have not chosen, the recipient(s) may not be able to read the 'message'. Run 'gpg --version' to find out which ciphers your G.P.G. version supports. This should also be avoided as it violates the P.G.P. standard. However, this does allow you to force the cipher used for your key generation and encrypting files, so it is useful if you wish to encrypt a file which only you will decrypt.

### digest-algo <single-string\>

Same as above, but for digests instead of ciphers. Run 'gpg --version' to find out which digests your G.P.G. version supports. 

### expert

Enable expert mode; this allows you to (for example) specify what type of capabilities keys you generate will have such as the ability to sign, certify, encrypt and authenticate. Note that this can also let you do things such as sign revoked keys. Think about this like running as root, it's powerful, but it WILL let you do stupid things, so be careful. 

### The s2k Options

**In some (older) versions of G.P.G. this will also be used when protecting the private key using a password, but newer verions ignore this enitrely, without notifying you. Because of this, be extremely careful with this option, and make sure that G.P.G. has actually used this cipher. HINT: Look into the 'list-packets' argument to do this.**

See the following links for discussion on this issue:

* [Mailing List Archive: {gnupg 2.1.6} Howto change s2k cipher from AES -> AES256?](https://lists.gt.net/gnupg/users/72518)
* [Archive.org Mirror - Mailing List Archive: {gnupg 2.1.6} Howto change s2k cipher from AES -> AES256?](https://web.archive.org/web/20170906064036/https://lists.gt.net/gnupg/users/72518)
* [⚓ T1800 Allow s2k options for gpg --export-secret-key](https://dev.gnupg.org/T1800)
* [Archive.org Mirror - ⚓ T1800 Allow s2k options for gpg --export-secret-key](https://web.archive.org/web/20170906064124/https://dev.gnupg.org/T1800)

#### s2k-cipher-algo

The algorithm to use when performing symmetric encryption (using the '-c' and '--symmetric' arguments). To find out what your version of G.P.G. supports, run 'gpg --version'. If this is not specified then the 'cipher-algo' and 'personal-cipher-preferences' options will be used instead. I recommend using 'aes256'.

#### s2k-digest-algo

The digest used to generate a hash used to verify a passphrase. Presumably, this option is used when creating a passphrase for a private key. I recommend using 'sha256'. 

###  The Meh of gpg.conf 

These are useful options which can be used in 'gpg.conf' which I once used in my configuration, but no longer use. Regardless, they are still worth knowing. 

#### default-key <key\>

G.P.G. 1 and 2 behave significantly differently when it comes to this option. [In G.P.G. 1, if you specify a default key and that key isn't available (say you failed to enter the password, or it just doesn't exist), then G.P.G. will exit with an error; nothing will be signed. In G.P.G. 2, if the specified key(s) are not available, G.P.G. will use the first secret key in the keyring.](#gpg-2-acknowledges-multiple-default-keys-gpg-1-does-not) Because of this, I recommend using the 'local-user' option instead since it will fail if all specified keys aren't available.

Note how I stated 'key(s)' when referring to v2.1.18. [Unlike v1.4.18, v2.1.18 allows you to specify more than one default key. When you have multiple default keys, the last key specified will be used, unless that key is unavailable.](#gpg-2-acknowledges-multiple-default-keys-gpg-1-does-not)

From the v1.4.18 Texinfo page:

    Use NAME as the default key to sign with. If this option is not used, the default key is the first key found in the secret keyring. Note that `-u' or `--local-user' overrides this option.

From the v2.1.18 Texinfo page:

    Use NAME as the default key to sign with. If this option is not used, the default key is the first key found in the secret keyring. Note that '-u' or '--local-user' overrides this option. This option may be given multiple times. In this case, the last key for which a secret key is available is used. If there is no secret key available for any of the specified values, GnuPG will not emit an error message but continue as if this option wasn't given. 

#### enable-large-rsa

Allow the user to generate R.S.A. keys larger that 4096 bits.

#### no-use-agent

Do not use the G.P.G. Agent at all. This option is ignored in G.P.G. 2 (at least under Debian Jessie - version 2.0.26-6+deb8u1 and Debian Stretch - version 2.1.18-6) as use of G.P.G. Agent is compulsory.

#### default-cert-level <integer\>

I have previously recommended setting this option when signing keys with trust signatures, but now believe it is best not to use this option to avoid accidentally setting the trust level of a key at a higher or lower level than intended. Instead, I recommend using the 'ask-cert-level' option instead so you are asked on each attempt. 


From the v2.1.18 Texinfo page:

    The default to use for the check level when signing a key.

    0 means you make no particular claim as to how carefully you verified the key.

    1 means you believe the key is owned by the person who claims to own it but you could not, or did not verify the key at all. This is useful for a "persona" verification, where you sign the key of a pseudonymous user.

    2 means you did casual verification of the key. For example, this could mean that you verified the key fingerprint and checked the user ID on the key against a photo ID.

    3 means you did extensive verification of the key. For example, this could mean that you verified the key fingerprint with the owner of the key in person, and that you checked, by means of a hard to forge document with a photo ID (such as a passport) that the name of the key owner matches the name in the user ID on the key, and finally that you verified (by exchange of email) that the email address on the key belongs to the key owner.

    Note that the examples given above for levels 2 and 3 are just that: examples. In the end, it is up to you to decide just what "casual" and "extensive" mean to you.

    This option defaults to 0 (no particular claim). 

#### armor

Always encode output such as public keys, private keys, signatures, encrypted data, etc using A.S.C.I.I. armor. This allows the output to be transmitted directly via e-mail, through instant messenger, forums, etc without having to 'upload' the file as an attachment.

I have previously recommended using this option in all cases since I couldn't think of a scenario where it would be a problem to base64-encode everything. In hindsight, this was a stupid recommendation as this can make encrypting large files extremely wasteful storage-wise. As a result, I do not recommend using this option in your configuration file unless you almost-always output G.P.G.-encrypted or signed data to a text-only medium (such as e-mail). 

## Demos

This section contains demonstrations showing that G.P.G. will act in the way stated in previous examples. This section has been placed at the bottom since it's irrelevant for most users and contains large bodies of text which would otherwise make the article difficult to read. 

###  Base Configuration 

All of this is done to ensure that my working environment for the demonstrations is as close to default as possible as well as prevent the spread of incorrect information about what G.P.G. does. To create these demos, I do the following:

#### Debian 8

    user@gpgref-jessie:~$ gpg --list-secret-keys
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --list-keys
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ ls -a ~/.gnupg/
    . .. gpg.conf pubring.gpg secring.gpg trustdb.gpg
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ echo -n > ~/.gnupg/gpg.conf
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ shasum -c 'Guide Test Key.asc.sha256'
    Guide Test Key.asc: OK
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ shasum -c Plain.txt.sha256
    Plain.txt: OK
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --import 'Guide Test Key.asc'
    gpg: key B980DC15: public key "Guide Test Key" imported
    gpg: key 36AC60C7: public key "Guide Test Key 2" imported
    gpg: key 19A2852D: public key "Guide Test Key 3" imported
    gpg: key B980DC15: secret key imported
    gpg: key B980DC15: "Guide Test Key" not changed
    gpg: key 36AC60C7: secret key imported
    gpg: key 36AC60C7: "Guide Test Key 2" not changed
    gpg: key 19A2852D: secret key imported
    gpg: key 19A2852D: "Guide Test Key 3" not changed
    gpg: Total number processed: 6
    gpg: imported: 3 (RSA: 3)
    gpg: unchanged: 3
    gpg: secret keys read: 3
    gpg: secret keys imported: 3
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --list-keys
    /home/user/.gnupg/pubring.gpg
    -----------------------------
    pub 2048R/B980DC15 2017-11-29
    uid Guide Test Key
    sub 2048R/2B2B4022 2017-11-29

    pub 2048R/36AC60C7 2017-11-29
    uid Guide Test Key 2
    sub 2048R/D94DA9C4 2017-11-29

    pub 2048R/19A2852D 2017-11-29
    uid Guide Test Key 3
    sub 2048R/F3E6E8E1 2017-11-29

    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --edit-key B980DC15
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    pub 2048R/B980DC15 created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    sub 2048R/2B2B4022 created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key

    gpg> trust
    pub 2048R/B980DC15 created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    sub 2048R/2B2B4022 created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key

    Please decide how far you trust this user to correctly verify other users' keys
    (by looking at passports, checking fingerprints from different sources, etc.)

    1 = I don't know or won't say
    2 = I do NOT trust
    3 = I trust marginally
    4 = I trust fully
    5 = I trust ultimately
    m = back to the main menu

    Your decision? 5
    Do you really want to set this key to ultimate trust? (y/N) y

    pub 2048R/B980DC15 created: 2017-11-29 expires: never usage: SC
    trust: ultimate validity: unknown
    sub 2048R/2B2B4022 created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key
    Please note that the shown key validity is not necessarily correct
    unless you restart the program.

    gpg> quit
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --edit-key 36AC60C7
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    pub 2048R/36AC60C7 created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    sub 2048R/D94DA9C4 created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 2

    gpg> trust
    pub 2048R/36AC60C7 created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    sub 2048R/D94DA9C4 created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 2

    Please decide how far you trust this user to correctly verify other users' keys
    (by looking at passports, checking fingerprints from different sources, etc.)

    1 = I don't know or won't say
    2 = I do NOT trust
    3 = I trust marginally
    4 = I trust fully
    5 = I trust ultimately
    m = back to the main menu

    Your decision? 5
    Do you really want to set this key to ultimate trust? (y/N) y

    pub 2048R/36AC60C7 created: 2017-11-29 expires: never usage: SC
    trust: ultimate validity: unknown
    sub 2048R/D94DA9C4 created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 2
    Please note that the shown key validity is not necessarily correct
    unless you restart the program.

    gpg> quit
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --edit-key 19A2852D
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    pub 2048R/19A2852D created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    sub 2048R/F3E6E8E1 created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 3

    gpg> trust
    pub 2048R/19A2852D created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    sub 2048R/F3E6E8E1 created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 3

    Please decide how far you trust this user to correctly verify other users' keys
    (by looking at passports, checking fingerprints from different sources, etc.)

    1 = I don't know or won't say
    2 = I do NOT trust
    3 = I trust marginally
    4 = I trust fully
    5 = I trust ultimately
    m = back to the main menu

    Your decision? 5
    Do you really want to set this key to ultimate trust? (y/N) y

    pub 2048R/19A2852D created: 2017-11-29 expires: never usage: SC
    trust: ultimate validity: unknown
    sub 2048R/F3E6E8E1 created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 3
    Please note that the shown key validity is not necessarily correct
    unless you restart the program.

    gpg> quit
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --edit-key B980DC15
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    pub 2048R/B980DC15 created: 2017-11-29 expires: never usage: SC
    trust: ultimate validity: ultimate
    sub 2048R/2B2B4022 created: 2017-11-29 expires: never usage: E
    [ultimate] (1). Guide Test Key

    gpg> quit
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --edit-key 36AC60C7
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    pub 2048R/36AC60C7 created: 2017-11-29 expires: never usage: SC
    trust: ultimate validity: ultimate
    sub 2048R/D94DA9C4 created: 2017-11-29 expires: never usage: E
    [ultimate] (1). Guide Test Key 2

    gpg> quit
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --edit-key 19A2852D
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    pub 2048R/19A2852D created: 2017-11-29 expires: never usage: SC
    trust: ultimate validity: ultimate
    sub 2048R/F3E6E8E1 created: 2017-11-29 expires: never usage: E
    [ultimate] (1). Guide Test Key 3

    gpg> quit
    user@gpgref-jessie:~$ 

#### Debian 9

    user@gpgref-stretch:~$ gpg --list-secret-keys
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --list-keys
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ ls -a ~/.gnupg/
    . .. private-keys-v1.d pubring.kbx trustdb.gpg
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ shasum -c 'Guide Test Key.asc.sha256'
    Guide Test Key.asc: OK
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ shasum -c Plain.txt.sha256
    Plain.txt: OK
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --import 'Guide Test Key.asc'
    gpg: key FE70696BB980DC15: public key "Guide Test Key" imported
    gpg: key 2C910B8436AC60C7: public key "Guide Test Key 2" imported
    gpg: key 1A8BF79319A2852D: public key "Guide Test Key 3" imported
    gpg: key FE70696BB980DC15: "Guide Test Key" not changed
    gpg: key FE70696BB980DC15: secret key imported
    gpg: key 2C910B8436AC60C7: "Guide Test Key 2" not changed
    gpg: key 2C910B8436AC60C7: secret key imported
    gpg: key 1A8BF79319A2852D: "Guide Test Key 3" not changed
    gpg: key 1A8BF79319A2852D: secret key imported
    gpg: Total number processed: 6
    gpg: imported: 3
    gpg: unchanged: 3
    gpg: secret keys read: 3
    gpg: secret keys imported: 3
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --list-keys
    /home/user/.gnupg/pubring.kbx
    -----------------------------
    pub rsa2048 2017-11-29 [SC]
    CCE598F97EC66DCAA07BF516FE70696BB980DC15
    uid [ unknown] Guide Test Key
    sub rsa2048 2017-11-29 [E]

    pub rsa2048 2017-11-29 [SC]
    3B79B5803061A415C9A31B1D2C910B8436AC60C7
    uid [ unknown] Guide Test Key 2
    sub rsa2048 2017-11-29 [E]

    pub rsa2048 2017-11-29 [SC]
    25BB75619B4A1714FEB453091A8BF79319A2852D
    uid [ unknown] Guide Test Key 3
    sub rsa2048 2017-11-29 [E]

    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --edit-key CCE598F97EC66DCAA07BF516FE70696BB980DC15
    gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    sec rsa2048/FE70696BB980DC15
    created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    ssb rsa2048/07AE0EEE2B2B4022
    created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key

    gpg> trust
    sec rsa2048/FE70696BB980DC15
    created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    ssb rsa2048/07AE0EEE2B2B4022
    created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key

    Please decide how far you trust this user to correctly verify other users' keys
    (by looking at passports, checking fingerprints from different sources, etc.)

    1 = I don't know or won't say
    2 = I do NOT trust
    3 = I trust marginally
    4 = I trust fully
    5 = I trust ultimately
    m = back to the main menu

    Your decision? 5
    Do you really want to set this key to ultimate trust? (y/N) y

    sec rsa2048/FE70696BB980DC15
    created: 2017-11-29 expires: never usage: SC
    trust: ultimate validity: unknown
    ssb rsa2048/07AE0EEE2B2B4022
    created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key
    Please note that the shown key validity is not necessarily correct
    unless you restart the program.

    gpg> quit
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --edit-key 3B79B5803061A415C9A31B1D2C910B8436AC60C7
    gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    gpg: checking the trustdb
    gpg: marginals needed: 3 completes needed: 1 trust model: pgp
    gpg: depth: 0 valid: 1 signed: 0 trust: 0-, 0q, 0n, 0m, 0f, 1u
    sec rsa2048/2C910B8436AC60C7
    created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    ssb rsa2048/1E0DF874D94DA9C4
    created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 2

    gpg> trust
    sec rsa2048/2C910B8436AC60C7
    created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    ssb rsa2048/1E0DF874D94DA9C4
    created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 2

    Please decide how far you trust this user to correctly verify other users' keys
    (by looking at passports, checking fingerprints from different sources, etc.)

    1 = I don't know or won't say
    2 = I do NOT trust
    3 = I trust marginally
    4 = I trust fully
    5 = I trust ultimately
    m = back to the main menu

    Your decision? 5
    Do you really want to set this key to ultimate trust? (y/N) y

    sec rsa2048/2C910B8436AC60C7
    created: 2017-11-29 expires: never usage: SC
    trust: ultimate validity: unknown
    ssb rsa2048/1E0DF874D94DA9C4
    created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 2
    Please note that the shown key validity is not necessarily correct
    unless you restart the program.

    gpg> quit
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --edit-key 25BB75619B4A1714FEB453091A8BF79319A2852D
    gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.

    Secret key is available.

    gpg: checking the trustdb
    gpg: marginals needed: 3 completes needed: 1 trust model: pgp
    gpg: depth: 0 valid: 2 signed: 0 trust: 0-, 0q, 0n, 0m, 0f, 2u
    sec rsa2048/1A8BF79319A2852D
    created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    ssb rsa2048/F5124797F3E6E8E1
    created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 3

    gpg> trust
    sec rsa2048/1A8BF79319A2852D
    created: 2017-11-29 expires: never usage: SC
    trust: unknown validity: unknown
    ssb rsa2048/F5124797F3E6E8E1
    created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 3

    Please decide how far you trust this user to correctly verify other users' keys
    (by looking at passports, checking fingerprints from different sources, etc.)

    1 = I don't know or won't say
    2 = I do NOT trust
    3 = I trust marginally
    4 = I trust fully
    5 = I trust ultimately
    m = back to the main menu

    Your decision? 5
    Do you really want to set this key to ultimate trust? (y/N) y

    sec rsa2048/1A8BF79319A2852D
    created: 2017-11-29 expires: never usage: SC
    trust: ultimate validity: unknown
    ssb rsa2048/F5124797F3E6E8E1
    created: 2017-11-29 expires: never usage: E
    [ unknown] (1). Guide Test Key 3
    Please note that the shown key validity is not necessarily correct
    unless you restart the program.

    gpg> quit
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --list-keys
    gpg: checking the trustdb
    gpg: marginals needed: 3 completes needed: 1 trust model: pgp
    gpg: depth: 0 valid: 3 signed: 0 trust: 0-, 0q, 0n, 0m, 0f, 3u
    /home/user/.gnupg/pubring.kbx
    -----------------------------
    pub rsa2048 2017-11-29 [SC]
    CCE598F97EC66DCAA07BF516FE70696BB980DC15
    uid [ultimate] Guide Test Key
    sub rsa2048 2017-11-29 [E]

    pub rsa2048 2017-11-29 [SC]
    3B79B5803061A415C9A31B1D2C910B8436AC60C7
    uid [ultimate] Guide Test Key 2
    sub rsa2048 2017-11-29 [E]

    pub rsa2048 2017-11-29 [SC]
    25BB75619B4A1714FEB453091A8BF79319A2852D
    uid [ultimate] Guide Test Key 3
    sub rsa2048 2017-11-29 [E]

    user@gpgref-stretch:~$ 

### Signing with 'local-user' Fails When All Specified Keys are Unavailable 

#### Debian 8

    user@gpgref-jessie:~$ gpg --verbose --local-user B980DC15 --local-user 36AC60C7 --sign Plain.txtgpg: writing to `Plain.txt.gpg'
    gpg: RSA/SHA1 signature from: "B980DC15 Guide Test Key"
    gpg: RSA/SHA1 signature from: "36AC60C7 Guide Test Key 2"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 16:54:29 AEDT using RSA key ID B980DC15
    gpg: Good signature from "Guide Test Key"
    gpg: Signature made Fri 01 Dec 2017 16:54:29 AEDT using RSA key ID 36AC60C7
    gpg: Good signature from "Guide Test Key 2"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --delete-secret-and-public-keys 36AC60C7
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    sec 2048R/36AC60C7 2017-11-29 Guide Test Key 2

    Delete this key from the keyring? (y/N) y
    This is a secret key! - really delete? (y/N) y

    pub 2048R/36AC60C7 2017-11-29 Guide Test Key 2

    Delete this key from the keyring? (y/N) y
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ rm Plain.txt.gpg
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verbose --local-user B980DC15 --local-user 36AC60C7 --sign Plain.txt
    gpg: skipped "36AC60C7": secret key not available
    gpg: writing to `Plain.txt.gpg'
    gpg: RSA/SHA1 signature from: "B980DC15 Guide Test Key"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 16:56:16 AEDT using RSA key ID B980DC15
    gpg: checking the trustdb
    gpg: 3 marginal(s) needed, 1 complete(s) needed, PGP trust model
    gpg: depth: 0 valid: 2 signed: 0 trust: 0-, 0q, 0n, 0m, 0f, 2u
    gpg: Good signature from "Guide Test Key"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --delete-secret-and-public-keys B980DC15
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    sec 2048R/B980DC15 2017-11-29 Guide Test Key

    Delete this key from the keyring? (y/N) y
    This is a secret key! - really delete? (y/N) y

    pub 2048R/B980DC15 2017-11-29 Guide Test Key

    Delete this key from the keyring? (y/N) y
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ rm Plain.txt.gpg
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verbose --local-user B980DC15 --local-user 36AC60C7 --sign Plain.txt
    gpg: skipped "36AC60C7": secret key not available
    gpg: skipped "B980DC15": secret key not available
    gpg: signing failed: secret key not available
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: can't open `Plain.txt.gpg'
    gpg: verify signatures failed: file open error
    user@gpgref-jessie:~$ 

#### Debian 9

    user@gpgref-stretch:~$ gpg --verbose --local-user CCE598F97EC66DCAA07BF516FE70696BB980DC15 --local-user 3B79B5803061A415C9A31B1D2C910B8436AC60C7 --sign Plain.txt
    gpg: writing to 'Plain.txt.gpg'
    gpg: RSA/SHA256 signature from: "FE70696BB980DC15 Guide Test Key"
    gpg: RSA/SHA256 signature from: "2C910B8436AC60C7 Guide Test Key 2"
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 16:03:45 AEDT
    gpg: using RSA key CCE598F97EC66DCAA07BF516FE70696BB980DC15
    gpg: Good signature from "Guide Test Key" [ultimate]
    gpg: Signature made Fri 01 Dec 2017 16:03:45 AEDT
    gpg: using RSA key 3B79B5803061A415C9A31B1D2C910B8436AC60C7
    gpg: Good signature from "Guide Test Key 2" [ultimate]
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --delete-secret-and-public-keys 3B79B5803061A415C9A31B1D2C910B8436AC60C7
    gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    sec rsa2048/2C910B8436AC60C7 2017-11-29 Guide Test Key 2

    Delete this key from the keyring? (y/N) y
    This is a secret key! - really delete? (y/N) y
    pub rsa2048/2C910B8436AC60C7 2017-11-29 Guide Test Key 2

    Delete this key from the keyring? (y/N) y
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ rm Plain.txt.gpg
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verbose --local-user CCE598F97EC66DCAA07BF516FE70696BB980DC15 --local-user 3B79B5803061A415C9A31B1D2C910B8436AC60C7 --sign Plain.txt
    gpg: skipped "3B79B5803061A415C9A31B1D2C910B8436AC60C7": No secret key
    gpg: writing to 'Plain.txt.gpg'
    gpg: RSA/SHA256 signature from: "FE70696BB980DC15 Guide Test Key"
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 16:04:30 AEDT
    gpg: using RSA key CCE598F97EC66DCAA07BF516FE70696BB980DC15
    gpg: Good signature from "Guide Test Key" [ultimate]
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ rm Plain.txt.gpg
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --delete-secret-and-public-keys CCE598F97EC66DCAA07BF516FE70696BB980DC15
    gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    sec rsa2048/FE70696BB980DC15 2017-11-29 Guide Test Key

    Delete this key from the keyring? (y/N) y
    This is a secret key! - really delete? (y/N) y
    pub rsa2048/FE70696BB980DC15 2017-11-29 Guide Test Key

    Delete this key from the keyring? (y/N) y
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verbose --local-user CCE598F97EC66DCAA07BF516FE70696BB980DC15 --local-user 3B79B5803061A415C9A31B1D2C910B8436AC60C7 --sign Plain.txt
    gpg: skipped "3B79B5803061A415C9A31B1D2C910B8436AC60C7": No secret key
    gpg: skipped "CCE598F97EC66DCAA07BF516FE70696BB980DC15": No secret key
    gpg: signing failed: No secret key
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: can't open 'Plain.txt.gpg': No such file or directory
    gpg: verify signatures failed: No such file or directory
    user@gpgref-stretch:~$ 

### 'local-user' Appends, Not Replaces

Using the 'local-user' arguments when a local user has been specified in 'gpg.conf' will add, not replace.

#### Debian 8 

    user@gpgref-jessie:~$ gpg --verbose --local-user B980DC15 --sign Plain.txt
    gpg: writing to `Plain.txt.gpg'
    gpg: RSA/SHA1 signature from: "B980DC15 Guide Test Key"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 16:36:28 AEDT using RSA key ID B980DC15
    gpg: Good signature from "Guide Test Key"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ echo 'local-user 36AC60C7' > ~/.gnupg/gpg.conf
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ rm Plain.txt.gpg
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verbose --local-user B980DC15 --sign Plain.txt
    gpg: writing to `Plain.txt.gpg'
    gpg: RSA/SHA1 signature from: "36AC60C7 Guide Test Key 2"
    gpg: RSA/SHA1 signature from: "B980DC15 Guide Test Key"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 16:38:10 AEDT using RSA key ID 36AC60C7
    gpg: Good signature from "Guide Test Key 2"
    gpg: Signature made Fri 01 Dec 2017 16:38:10 AEDT using RSA key ID B980DC15
    gpg: Good signature from "Guide Test Key"
    user@gpgref-jessie:~$ 

#### Debian 9

    user@gpgref-stretch:~$ gpg --verbose --local-user CCE598F97EC66DCAA07BF516FE70696BB980DC15 --sign Plain.txt
    gpg: writing to 'Plain.txt.gpg'
    gpg: RSA/SHA256 signature from: "FE70696BB980DC15 Guide Test Key"
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 16:23:53 AEDT
    gpg: using RSA key CCE598F97EC66DCAA07BF516FE70696BB980DC15
    gpg: Good signature from "Guide Test Key" [ultimate]
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ echo 'local-user 3B79B5803061A415C9A31B1D2C910B8436AC60C7' > ~/.gnupg/gpg.conf
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ rm Plain.txt.gpg
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verbose --local-user CCE598F97EC66DCAA07BF516FE70696BB980DC15 --sign Plain.txt
    gpg: writing to 'Plain.txt.gpg'
    gpg: RSA/SHA256 signature from: "2C910B8436AC60C7 Guide Test Key 2"
    gpg: RSA/SHA256 signature from: "FE70696BB980DC15 Guide Test Key"
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 16:25:01 AEDT
    gpg: using RSA key 3B79B5803061A415C9A31B1D2C910B8436AC60C7
    gpg: Good signature from "Guide Test Key 2" [ultimate]
    gpg: Signature made Fri 01 Dec 2017 16:25:01 AEDT
    gpg: using RSA key CCE598F97EC66DCAA07BF516FE70696BB980DC15
    gpg: Good signature from "Guide Test Key" [ultimate]
    user@gpgref-stretch:~$ 

### G.P.G. 1 vs G.P.G. 2 Action When Missing Default Key

When using G.P.G. 1, if the key specified with 'default-key' is missing, G.P.G. will quit with an error stating that the default key is missing. When using G.P.G. 2, if the specified 'default-key' is missing, G.P.G. will use the first available secret key in the keyring.

#### Debian 8 

    user@gpgref-jessie:~$ gpg --verbose --default-key 19A2852D --sign Plain.txt
    gpg: writing to `Plain.txt.gpg'
    gpg: RSA/SHA1 signature from: "19A2852D Guide Test Key 3"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 17:26:25 AEDT using RSA key ID 19A2852D
    gpg: Good signature from "Guide Test Key 3"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --delete-secret-and-public-keys 19A2852D
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    sec 2048R/19A2852D 2017-11-29 Guide Test Key 3

    Delete this key from the keyring? (y/N) y
    This is a secret key! - really delete? (y/N) y

    pub 2048R/19A2852D 2017-11-29 Guide Test Key 3

    Delete this key from the keyring? (y/N) y
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ rm Plain.txt.gpg
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verbose --default-key 19A2852D --sign Plain.txt
    gpg: no default secret key: secret key not available
    gpg: signing failed: secret key not available
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: can't open `Plain.txt.gpg'
    gpg: verify signatures failed: file open error
    user@gpgref-jessie:~$ 

#### Debian 9

    user@gpgref-stretch:~$ gpg --verbose --default-key 25BB75619B4A1714FEB453091A8BF79319A2852D --sign Plain.txt
    gpg: using pgp trust model
    gpg: using "25BB75619B4A1714FEB453091A8BF79319A2852D" as default secret key for signing
    gpg: writing to 'Plain.txt.gpg'
    gpg: RSA/SHA256 signature from: "1A8BF79319A2852D Guide Test Key 3"
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 17:36:02 AEDT
    gpg: using RSA key 25BB75619B4A1714FEB453091A8BF79319A2852D
    gpg: Good signature from "Guide Test Key 3" [ultimate]
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --delete-secret-and-public-keys 25BB75619B4A1714FEB453091A8BF79319A2852D
    gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    sec rsa2048/1A8BF79319A2852D 2017-11-29 Guide Test Key 3

    Delete this key from the keyring? (y/N) y
    This is a secret key! - really delete? (y/N) y
    pub rsa2048/1A8BF79319A2852D 2017-11-29 Guide Test Key 3

    Delete this key from the keyring? (y/N) y
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ rm Plain.txt.gpg
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verbose --default-key 25BB75619B4A1714FEB453091A8BF79319A2852D --sign Plain.txt
    gpg: all values passed to '--default-key' ignored
    gpg: using pgp trust model
    gpg: writing to 'Plain.txt.gpg'
    gpg: RSA/SHA256 signature from: "FE70696BB980DC15 Guide Test Key"
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 17:37:16 AEDT
    gpg: using RSA key CCE598F97EC66DCAA07BF516FE70696BB980DC15
    gpg: Good signature from "Guide Test Key" [ultimate]
    user@gpgref-stretch:~$ 

###  G.P.G. 2 Acknowledges Multiple Default Keys, G.P.G. 1 Does Not 

When multiple default keys are specified in G.P.G. 2, it will use the last key in the order specified which is available, but only one key will be used. If the last key is unavailable, it will use the key specified before that. If all specified keys are unavailable, it will use the first available secret key in the keyring. When multiple default keys are specified in G.P.G. 1, it will use the last key in the order specified, and fail if this key is unavailable.

#### Debian 8 

    user@gpgref-jessie:~$ gpg --verbose --default-key 36AC60C7 --default-key 19A2852D --sign Plain.txtgpg: writing to `Plain.txt.gpg'
    gpg: RSA/SHA1 signature from: "19A2852D Guide Test Key 3"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 19:03:27 AEDT using RSA key ID 19A2852D
    gpg: Good signature from "Guide Test Key 3"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --delete-secret-and-public-keys 19A2852D
    gpg (GnuPG) 1.4.18; Copyright (C) 2014 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    sec 2048R/19A2852D 2017-11-29 Guide Test Key 3

    Delete this key from the keyring? (y/N) y
    This is a secret key! - really delete? (y/N) y

    pub 2048R/19A2852D 2017-11-29 Guide Test Key 3

    Delete this key from the keyring? (y/N) y
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ rm Plain.txt.gpg
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verbose --default-key 36AC60C7 --default-key 19A2852D --sign Plain.txt
    gpg: no default secret key: secret key not available
    gpg: signing failed: secret key not available
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: can't open `Plain.txt.gpg'
    gpg: verify signatures failed: file open error
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verbose --default-key 36AC60C7 --sign Plain.txt
    gpg: writing to `Plain.txt.gpg'
    gpg: RSA/SHA1 signature from: "36AC60C7 Guide Test Key 2"
    user@gpgref-jessie:~$
    user@gpgref-jessie:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 19:06:49 AEDT using RSA key ID 36AC60C7
    gpg: checking the trustdb
    gpg: 3 marginal(s) needed, 1 complete(s) needed, PGP trust model
    gpg: depth: 0 valid: 2 signed: 0 trust: 0-, 0q, 0n, 0m, 0f, 2u
    gpg: Good signature from "Guide Test Key 2"
    user@gpgref-jessie:~$ 

####  Debian 9 

    user@gpgref-stretch:~$ gpg --verbose --default-key 3B79B5803061A415C9A31B1D2C910B8436AC60C7 --default-key 25BB75619B4A1714FEB453091A8BF79319A2852D --sign Plain.txt
    gpg: using pgp trust model
    gpg: using "25BB75619B4A1714FEB453091A8BF79319A2852D" as default secret key for signing
    gpg: writing to 'Plain.txt.gpg'
    gpg: RSA/SHA256 signature from: "1A8BF79319A2852D Guide Test Key 3"
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 18:57:40 AEDT
    gpg: using RSA key 25BB75619B4A1714FEB453091A8BF79319A2852D
    gpg: Good signature from "Guide Test Key 3" [ultimate]
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --delete-secret-and-public-keys 25BB75619B4A1714FEB453091A8BF79319A2852D
    gpg (GnuPG) 2.1.18; Copyright (C) 2017 Free Software Foundation, Inc.
    This is free software: you are free to change and redistribute it.
    There is NO WARRANTY, to the extent permitted by law.


    sec rsa2048/1A8BF79319A2852D 2017-11-29 Guide Test Key 3

    Delete this key from the keyring? (y/N) y
    This is a secret key! - really delete? (y/N) y
    pub rsa2048/1A8BF79319A2852D 2017-11-29 Guide Test Key 3

    Delete this key from the keyring? (y/N) y
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ rm Plain.txt.gpg
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verbose --default-key 3B79B5803061A415C9A31B1D2C910B8436AC60C7 --default-key 25BB75619B4A1714FEB453091A8BF79319A2852D --sign Plain.txt
    gpg: using pgp trust model
    gpg: using "3B79B5803061A415C9A31B1D2C910B8436AC60C7" as default secret key for signing
    gpg: writing to 'Plain.txt.gpg'
    gpg: RSA/SHA256 signature from: "2C910B8436AC60C7 Guide Test Key 2"
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 18:58:22 AEDT
    gpg: using RSA key 3B79B5803061A415C9A31B1D2C910B8436AC60C7
    gpg: checking the trustdb
    gpg: marginals needed: 3 completes needed: 1 trust model: pgp
    gpg: depth: 0 valid: 2 signed: 0 trust: 0-, 0q, 0n, 0m, 0f, 2u
    gpg: Good signature from "Guide Test Key 2" [ultimate]
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ rm Plain.txt.gpg
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verbose --default-key 25BB75619B4A1714FEB453091A8BF79319A2852D --sign Plain.txt
    gpg: all values passed to '--default-key' ignored
    gpg: using pgp trust model
    gpg: writing to 'Plain.txt.gpg'
    gpg: RSA/SHA256 signature from: "FE70696BB980DC15 Guide Test Key"
    user@gpgref-stretch:~$
    user@gpgref-stretch:~$ gpg --verify Plain.txt.gpg
    gpg: Signature made Fri 01 Dec 2017 18:59:43 AEDT
    gpg: using RSA key CCE598F97EC66DCAA07BF516FE70696BB980DC15
    gpg: Good signature from "Guide Test Key" [ultimate]
    user@gpgref-stretch:~$ 
