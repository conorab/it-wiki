# Useful Links

## Unsorted

* [File System Pass-Through in KVM/Qemu/libvirt](https://troglobit.github.io/2013/07/05/file-system-pass-through-in-kvm-slash-qemu-slash-libvirt/)
* [Tunneling SSH over HTTP(S) ](http://dag.wiee.rs/howto/ssh-http-tunneling/)
* [Use PowerShell to Create Scheduled Tasks](https://devblogs.microsoft.com/scripting/use-powershell-to-create-scheduled-tasks/)
* [How To Setup DNSSEC on an Authoritative BIND DNS Server](https://www.digitalocean.com/community/tutorials/how-to-setup-dnssec-on-an-authoritative-bind-dns-server--2)
* [Creating VLAN Bridges with systemd-networkd](https://www.variantweb.net/blog/creating-vlan-bridges-with-systemd-networkd/)
* [What are Amazon AWS vCPUs?](https://samrueby.com/2015/01/12/what-are-amazon-aws-vcpus/)
* [Adding an extra disk to an mdadm array](https://zackreed.me/adding-an-extra-disk-to-an-mdadm-array/)
* [Preserving optical media from the command line | KB Research](http://web.archive.org/web/20160209064316/http://blog.kbresearch.nl/2015/11/13/preserving-optical-media-from-the-command-line/)
* [Window Server 2016 - Configuring Radius Server](https://weipin886.blogspot.com/2016/11/window-server-2016-configuring-radius.html)
* [GitHub - kaduke/Netgear-A6210: AC1200 High Gain WiFi USB Adapter Linux kernel driver ](https://github.com/kaduke/Netgear-A6210)
* [GitHub - lwfinger/rtl8188eu: Repository for stand-alone RTL8188EU driver.](https://github.com/lwfinger/rtl8188eu)
* [Set up Auto Logon in Windows using PowerShell](http://www.sysadmins.eu/2014/12/set-up-auto-logon-in-windows-using.html)
* [Running WordPress Behind an SSL/HTTPS Terminating Proxy - Feliciano.Tech](https://feliciano.tech/blog/running-wordpress-behind-an-sslhttps-terminating-proxy/)
* [io - How to copy CDROM to ISO in Debian 8.x? - Unix & Linux Stack Exchange](https://unix.stackexchange.com/questions/311365/how-to-copy-cdrom-to-iso-in-debian-8-x)
* [iptables not filtering bridged traffic](https://unix.stackexchange.com/questions/410112/iptables-not-filtering-bridged-traffic)
* [How to Setup DKIM (DomainKeys) with Postfix on Ubuntu & Debian](https://tecadmin.net/setup-dkim-with-postfix-on-ubuntu-debian/)

## Setting Up DNSSEC

* Out-of-date article that recommends SHA-1 signature but has useful commands: [How to Setup DNSSEC on an Authoritative BIND DNS Server | Digital Ocean](https://www.digitalocean.com/community/tutorials/how-to-setup-dnssec-on-an-authoritative-bind-dns-server-2)
* Contains insturctions using SHA-256: [Debian Wiki: DNSSEC Howto for BIND 9.9+](https://wiki.debian.org/DNSSEC%20Howto%20for%20BIND%209.9+)

## DNS Testing

* Test client DNSSEC support: [DNSSEC Resolver Test](http://dnssec.vs.uni-due.de/)
* Flush record Google's public DNS servers: [Flush Cache](https://developers.google.com/speed/public-dns/cache)
* Query Google's public DNS via HTTPS: [Google Public DNS](https://dns.google.com/)
* Test DNSSEC configuration in your DNS server: [DNSViz | A DNS visualization tool](https://dnsviz.net/)
* Test DNSSEC configuration in your DNS server: [DNSSEC Analyzer](https://dnssec-analyzer.verisignlabs.com/)

## E-mail Configuration

* [NFS - Dovecot Wiki](https://wiki2.dovecot.org/NFS)

## E-mail Testing

* Test DKIM Configuration: [DKIM, SPF, SpamAssassin Email Validator](https://dkimvalidator.com/)

## Oracle Java Checksums

* Java SE 8u201 Binaries Checksum: [Java SE Binaries Checksum](https://www.oracle.com/webfolder/s/digest/8u201checksum.html)

## Minecraft

* Change chunk light population via MCEdit: [Chunk properties editor](https://www.mediafire.com/file/ra8571au9j9axco/Chunk_properties_editor.py/file)

## Linux Tools

* Find files opened by process: [perf-tools/opensnoop at master · brendangregg/perf-tools · GitHub](https://github.com/brendangregg/perf-tools/blob/master/opensnoop)
