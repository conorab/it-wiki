# Samba Configs - smb-1.conf

	[global]

	# R.T.F.M.
	server role = standalone server
	security = user

	# Use the generic workgroup.
	# You may need to specify this when logging in if you have any issues.
	# This may also be referred to as the 'domain' on the client.
	workgroup = WORKGROUP

	# Do not allow the Samba D.N.S. server to be have it's records updated.
	allow dns updates = disabled

	# Don't forward D.N.S. requests from Samba to another server.
	dns forwarder =

	# Don't follow symbolic links since this allows users to symlink to files like /etc/passwd even though it's not in the share.
	follow symlinks = no

	# Force newly created files to have a mode of 0664.
	force create mode = 0664

	# Force newely create directories to have a mode of 0775.
	force directory mode = 0775

	# The guest account.
	guest account = anonymous

	# By default, do not allow guest access to shares.
	guest ok = no

	# Do not perform hostname lookups.
	hostname lookups = no

	# By default do not allow these users or groups (groups start with '@') to connect to shares.
	invalid users = root @wheel @admin

	# Don't allow clients to authenticate using 'LANMAN'.
	lanman auth = no

	# Don't let users create symbolic links to paths outside the shares.
	allow insecure wide links = no

	# Allow locks on regions of files to be obtained.
	blocking locks = yes

	# Honors requests to lock files; don't lie.
	locking = yes

	# In the event that a client loses connection and is writing files, log all the files which are being written to.
	# This gives us a list of files which may be corrupt due to incomplete writes by a client that lost connection.
	log writeable files on exit = yes

	# Do not announce the existence of the Samba server over multicast D.N.S.
	# This is to prevent Mac and Windows users from seeing the server automatically come up on the side bar or in network places, and forces users to manually enter the server address.
	multicast dns register = no

	# By default, issue oportunistic locks for files so that clients can cache files more frequently and therefor increase performance.
	oplocks = yes

	# Before performing any read or write, the server will check if the file is locked.
	# If it is, the server will deny access.
	strict locking = yes

	# When attempting to rename a file, check if any sub-files or directories are open.
	# If so, deny.
	strict rename = yes

	# If a client requests that all data is written to disk, do it.
	strict sync = yes

	# Do not ensure that all data is flushed to disk before a write request is completed.
	# This is the default, but we're specifying it here to display our intent.
	# We have overwritten other defaults to ensure that if a client explicitly requests to sync, we do it, but if the client doesn't think the data is worth syncing, we won't waste the resources.
	sync always = no

	# Do not follow symbolic links which are outside the exported (shared) directory.
	wide links = no

	# Do not act as a 'WINS' server.
	wins support = no

	# Send almost all logs to JournalD.
	logging = systemd

	# Don't use 'NetBIOS'.
	disable netbios = yes

	# Fail any failed login attempts for any reason and do not give them guest access without explicitly trying to login as the guest.
	map to guest = Never

	# We're not including access to home folders here.
	# This is done by not including the '[homes]' section, as we're commented out below.
	#[homes]

	# Debian Stretch Defaults/Debian-specific password change fixes.
	passdb backend = tdbsam
	obey pam restrictions = yes
	unix password sync = yes
	passwd program = /usr/bin/passwd %u
	passwd chat = *Enter\snew\s*\spassword:* %n\n *Retype\snew\s*\spassword:* %n\n *password\supdated\ssuccessfully* .
	pam password change = yes

	[General]
	path = /netshares/General
	writable = yes
	guest ok = no
	force create mode = 660
	force directory mode = 770
	force group = users
	valid users = user1

	[hostshare1]
	path = /netshares/hostshare1
	writeable = yes
	guest ok = yes
	force create mode = 660
	force directory mode = 770
	force group = anonymous
	valid users = anonymous
	hosts allow = 192.168.0.2

## Import Note
 
This article was originally written on 2020-05-14 18:57Z+10 and moved from [Conor Buckley's Wiki](https://www.conorab.com/wiki/) to here on 2021-01-03 1948Z+11.
