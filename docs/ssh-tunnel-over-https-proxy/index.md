# SSH Tunnel Over HTTPS Proxy

## Assumptions

* This guide was built with Debian 10 in mind as the client and server OS.
* The SSH and Apache2 configurations require a domain be ready and pointing to the server which will be the HTTP proxy; in this guide "proxy.example.com" is used.
* The SSH server is already setup on the machine we are proxying to; in this guide this machine is "hidden-server.example.com".
* The SSH client is already installed on the client machine.
* BSD Netcat is installed on the client machine and can be run using "nc".

## Placeholder addresses

* The address of the server you wish to connect to: "hidden-server.example.com".
* The internal address of the same server: "hidden-server-internal.example.com".
* The address of the HTTPS proxy server: "proxy.example.com".
* The email address Let's Encrypt will send expiry notifications to for your certificate: "hostmaster@example.com".
* The location of the certificate authority certificate (not key) which the HTTPS proxy server uses: "/home/user/.ssh/letsencrypt.pem".
* The username used to authenticate with the HTTPS proxy: "myusername".
* The password used to authenticate with the HTTPS proxy: "mypassword".

## Justification

Using an HTTPS proxy with SSH helps prevent brute-force password attacks and provides some extra protection against OpenSSH exploits if one is found, as the attacker would need to be coming from either one of the white-listed IPs, the machine running the HTTPS proxy or need to obtain the credentials for the HTTPS proxy. For example: "hidden-server.example.com" is a hypervisor with an external IP. This hypervisor runs a web server VM which runs HTTPS proxy (in addition to other websites). SSH access to the hypervisor from non-white-listed IPs is blocked using IPTables. When connecting to the hypervisor from home (a white-listed IP), the client connects directly to the hypervisor. If this client is connecting from their mobile phone Wi-Fi, the client detects it cannot reach the hypervisor directly and connect via the HTTPS proxy instead.

This can be preferable to a VPN as it will not affect any other applications on your computer (none of your routes have changed) or prevent you from connecting to a VPN at the same time. An extra benefit to this approach is that unless the web filter is blocking your website or doing DPI then this traffic will appear to be normal HTTPS traffic; though the logs will show you attempt to connect via the SSH port in the first instance.

## Setup the HTTPS Proxy Server

Install Apache2 and the Apache2 plugin for Certbot so certificates can be renewed automatically:

	sudo apt -y update
	sudo apt -y install apache2 python3-certbot-apache

Configure the proxy website in Apache2:

	sudo nano /etc/apache2/sites-available/proxy.conf

Enter the following into the configuration:

	<VirtualHost *:443>
		ServerName proxy.example.com

		CustomLog ${APACHE_LOG_DIR}/access.log combined

		ProxyRequests on
		AllowCONNECT 22 2022
		ProxyVia on

		<Proxy *>
			AuthType Basic
			AuthName "Authentication Required"
			AuthUserFile "/etc/apache2/proxy.passwd"
			Require valid-user
		</proxy>

		SSLEngine on
		SSLCertificateFile /etc/letsencrypt/live/proxy.example.com/fullchain.pem
		SSLCertificateKeyFile /etc/letsencrypt/live/proxy.example.com/privkey.pem
	</VirtualHost>
	# vim: syntax=apache ts=4 sw=4 sts=4 sr noet

Enable the new website configuration:

	sudo a2ensite proxy.conf

Disable the default websites:

	sudo a2dissite 000-default
	sudo a2dissite default-ssl

Stop Apache2 and obtain the certificate for the HTTPS proxy.

	sudo systemctl stop apache2

	sudo certbot certonly --agree-tos --keep-until-expiring --non-interactive --standalone -d proxy.example.com --email hostmaster@example.com --preferred-challenges http

Create the user "myusername" with the password given at the prompt:

	sudo htpasswd -Bc /etc/apache2/proxy.passwd myusername

Enable the Apache2 modules needed to run the HTTPS proxy:

	sudo a2enmod auth_basic ssl proxy_connect proxy_http

Restart Apache2:

	sudo systemctl restart apache2

Confirm the HTTPS proxy is running:

	sudo systemctl status apache2

If the output says the server is running then the configuration should be correct.

## Setup the Client

Install "proxytunnel":

	sudo apt update
	sudo apt -y install proxytunnel

Include the following in your ~/.ssh/config file:

	Host hidden-server.example.com
	  Match exec "nc -zG 1 hidden-server.example.com %p" originalhost hidden-server.example.com
	    HostName hidden-server.example.com
	  Match !exec "nc -zG 1 hidden-server.example.com %p" originalhost hidden-server.example.com
	    HostName hidden-server-internal.example.com
	    ProxyCommand proxytunnel -v -p proxy.example.com:443 -E -d hidden-server-internal.example.com:22 -H "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Win32)\n" -P myusername:mypassword -C /home/user/.ssh/letsencrypt.pem

The following lines in this configuration do the following:

1. Only apply these settings when trying to SSH to "hidden-server.example.com".
2. Apply line 3 if the SSH port on "hidden-server.example.com" is accessible (try and connect to it with "nc").
3. Tell SSH to connect to "hidden-server.example.com".
4. Apply lines 5-6 if the SSH port on "hidden-server.example.com" is not accessible.
5. Tell SSH to connect to "hidden-server-internal.example.com" instead of the address provided to SSH. This is an address which the HTTPS proxy can connect to but we can't; e.g. because the address is behind a firewall.
6. Execute "proxytunnel" and use it to communicate with the SSH server instead.

This SSH configuration causes your machine to check if "hidden-server.example.com" responds TCP connections of port 22 every time you run SSH regardless of whether you are connecting to "hidden-server.example.com". If it does respond, then SSH will connect to the server directly using the host given on line 3. If it can't reach the server, then it will apply lines 5-6 and so your machine will connect to the HTTPS proxy and connect to "hidden-server-internal.example.com" instead through the proxy. The benefit of this is you can whitelist which IP addresses can connect to "hidden-server.example.com" with firewall rules so only a small set of IPs can connect to it. However, if you are not coming from one of those IP addresses, your machine will see that the connection has failed and connect to "hidden-server-internal.example.com" (an internal address only reachable behind a firewall) through the proxy instead. Note that while SSH will always check if it can reach "hidden-server.example.com", it will not apply any changes unless you are telling SSH to connect to that server.

